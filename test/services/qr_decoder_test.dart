import 'package:dpatrimony/models/beneficiary.dart';
import 'package:dpatrimony/models/beneficiary_status.dart';
import 'package:dpatrimony/models/secret.dart';
import 'package:dpatrimony/services/qr_decoder.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  const String secretJson1 = '''
  {
  "n":"Mary",
  "t":"ledger seed 1",
  "d":"family",
  "s":"orph deny reje key obse pig pist mist padd cost obli deri eleg cere hosp",
  "w":0
  }
  ''';

  const String secretJson2 = '''
  {
  "n":"Mary",
  "t":"ledger seed 2",
  "d":"family",
  "s":"orph deny reje key obse pig pist mist padd cost obli deri eleg cere aban",
  "w":0
  }
  ''';

  TestWidgetsFlutterBinding.ensureInitialized();

  Beneficiary decode(String json) {
    String encodedData = QrDecoder.encodeJsonToQr(json);
    Beneficiary beneficiary = QrDecoder.decodeQrString(encodedData);
    return beneficiary;
  }

  test('Ensure QR code data can be loaded into a beneficiary with secrets', () async {
    Beneficiary beneficiary1 = decode(secretJson1);

    expect(beneficiary1.status, BeneficiaryStatus.pending_setup);
    expect(beneficiary1.message, '');
    expect(beneficiary1.email, '');
    expect(beneficiary1.isEmailValid, false);

    expect(beneficiary1.name, 'Mary');
    expect(beneficiary1.secrets.length, 1);

    Beneficiary beneficiary2 = decode(secretJson2);
    expect(beneficiary1, beneficiary2);
    expect(beneficiary1.secrets.first, isNot(equals(beneficiary2.secrets.first)));
    beneficiary1.merge(beneficiary2);
    expect(beneficiary1.secrets.length, 2);

    Secret secret1 = beneficiary1.secrets.first;
    expect(secret1.dictionary, 'family');
    expect(secret1.description, 'ledger seed 1');
    expect(await secret1.getWordList(),
        ['orphan', 'deny', 'reject', 'key', 'observe', 'pig', 'pistol', 'mistake', 'paddle', 'cost', 'oblige', 'derive', 'elegant', 'cereal', 'hospital']);

    Secret secret2 = beneficiary1.secrets.last;
    expect(secret2.dictionary, 'family');
    expect(secret2.description, 'ledger seed 2');

    expect(await secret2.getWordList(),
        ['orphan', 'deny', 'reject', 'key', 'observe', 'pig', 'pistol', 'mistake', 'paddle', 'cost', 'oblige', 'derive', 'elegant', 'cereal', 'abandon']);
  });
}
