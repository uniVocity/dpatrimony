import 'package:dpatrimony/models/word_list.dart';
import 'package:dpatrimony/services/secret_service.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  test('Word list should be loaded and words restored from their first 4 characters', () async {
    final service = SecretService();

    List<String> words;
    words = await service.expandSecretWords('abc', WordList.English);
    expect(words.length, 0);

    words = await service.expandSecretWords('aban ask bicy', WordList.English);
    expect(words.length, 3);
    expect(words, ['abandon', 'ask', 'bicycle']);
  });

  test('ensure all languages are accessible', () async {
    final service = SecretService();

    for (WordList list in WordList.values) {
      String words = await service.loadWordListFile(list);
      expect(words, isNot(null));
      expect(words.length > 100, true);
    }
  });
}
