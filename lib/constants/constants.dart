import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:sizer/sizer.dart';

const Color colorMain = Color(0xFF222222);

final Color colorCameraHud = colorMain;

double font8 = 6.15.sp;
double font10 = 7.5.sp;
double font12 = 9.2.sp;
double font14 = 10.7.sp;
double font16 = 12.25.sp;
double font18 = 13.8.sp;
double font20 = 15.3.sp;
double font34 = 22.5.sp;
double font36 = 27.5.sp;

TextStyle h1Style = GoogleFonts.openSans(
  fontWeight: FontWeight.w900,
  fontSize: font36,
  color: Color(0xFF1F232C),
  letterSpacing: -2,
);

final TextStyle h2Style = h1Style.copyWith(
  fontWeight: FontWeight.w700,
  fontSize: font18,
  letterSpacing: 0,
);

TextStyle h3Style = GoogleFonts.openSans(
  fontWeight: FontWeight.w400,
  fontSize: font16,
  color: Color(0xFF474B54),
);

TextStyle h4Style = GoogleFonts.openSans(
  fontWeight: FontWeight.w400,
  fontSize: font14,
  color: Color(0xFF1F232C),
);

final TextStyle styleLabel = TextStyle(fontWeight: FontWeight.w400, fontSize: font14, color: colorMain);
final TextStyle styleValue = styleLabel.copyWith(color: Color(0xFF1F232C));
final TextStyle styleReadOnly = styleLabel.copyWith(fontWeight: FontWeight.w600);
final TextStyle styleCardTitle = styleReadOnly.copyWith(fontSize: font18);
final TextStyle styleSectionTitle = styleReadOnly.copyWith(fontSize: font20);
final TextStyle linkStyle = h3Style.copyWith(color: Color(0xFF3492FF));
final TextStyle styleInputLabel = styleValue.copyWith(fontSize: font12);

final Icon dictionaryIcon = Icon(
  Icons.book,
  size: font14,
  color: colorMain,
);

final Icon acceptIcon = Icon(
  Icons.check,
  size: font14,
  color: colorMain,
);

final Icon payIcon = Icon(
  Icons.attach_money,
  size: font14,
  color: colorMain,
);

final Icon deleteIcon = Icon(
  Icons.clear,
  size: font14,
  color: Colors.redAccent,
);

final Icon saveIcon = Icon(
  Icons.save,
  size: font14,
  color: colorMain,
);

final Icon backIcon = Icon(
  Icons.arrow_back,
  size: font14,
  color: colorMain,
);

final Icon homeIcon = Icon(
  Icons.home,
  size: font14,
  color: colorMain,
);

final BoxShadow tableHeaderBoxShadow = new BoxShadow(color: Color(0xFF3C485B), blurRadius: 1.0, offset: new Offset(-0.5, -1.0));
final BoxShadow labelBoxShadow = new BoxShadow(color: Colors.white54, blurRadius: 1.0, offset: new Offset(-0.5, 1.0));
final BoxShadow raisedIconButtonBoxShadow = new BoxShadow(color: Colors.white54, blurRadius: 1.0, offset: new Offset(-0.5, 1.0));
final BoxShadow pressedIconButtonBoxShadow = new BoxShadow(color: Colors.grey, blurRadius: 1.0, offset: new Offset(-0.5, 0.5));

final BoxDecoration columnTopMiddleDecoration = BoxDecoration(
  color: Color(0xFF3C485B),
  boxShadow: [tableHeaderBoxShadow],
);

Radius radius10 = Radius.circular(10.0);

final BoxDecoration cardTopDecoration = columnTopMiddleDecoration.copyWith(
  borderRadius: BorderRadius.only(topLeft: radius10, topRight: radius10),
);

final BoxDecoration columnTopLeftDecoration = columnTopMiddleDecoration.copyWith(
  borderRadius: BorderRadius.only(topLeft: radius10),
);

final BoxDecoration columnTopRightDecoration = columnTopMiddleDecoration.copyWith(
  borderRadius: BorderRadius.only(topRight: radius10),
);

final BoxShadow cardBottomBoxShadow = new BoxShadow(color: Colors.blueGrey, blurRadius: 0.0, offset: new Offset(-0.5, 0.0));

final BoxDecoration columnMiddleDecoration = BoxDecoration(
  color: Color(0xFF1F232C).withOpacity(0.08),
);

final BoxDecoration columnBottomMiddleDecoration = columnMiddleDecoration.copyWith();

final BoxDecoration columnBottomLeftDecoration = columnMiddleDecoration.copyWith(
  borderRadius: BorderRadius.only(bottomLeft: radius10),
);

final BoxDecoration columnBottomRightDecoration = columnMiddleDecoration.copyWith(
  borderRadius: BorderRadius.only(bottomRight: radius10),
);

final BoxDecoration bottomRowDecoration = columnMiddleDecoration.copyWith(
  borderRadius: BorderRadius.only(bottomLeft: radius10, bottomRight: radius10),
);

final kTextFieldDecoration = InputDecoration(
  hintText: '',
  contentPadding: EdgeInsets.symmetric(vertical: font8, horizontal: font18),
  border: OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(font8)),
  ),
  enabledBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Color(0xFFC9CDD6), width: 1.0),
    borderRadius: BorderRadius.all(Radius.circular(font8)),
  ),
  focusedBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Color(0xFFC9CDD6), width: 2.0),
    borderRadius: BorderRadius.all(Radius.circular(font8)),
  ),
);

TextStyle kTextStyle = TextStyle(
  color: Colors.white,
  fontSize: font20,
  fontWeight: FontWeight.normal,
);

final BoxDecoration containerButton = BoxDecoration(
  borderRadius: BorderRadius.circular(5.0),
  border: Border.all(
    color: Colors.white,
    width: 1.5,
  ),
);

final TextStyle kButtonActiveColorOption = TextStyle(
  color: Colors.cyan.shade900,
  fontSize: font20,
);

final TextStyle kButtonInactiveColorOption = styleLabel.copyWith(
  fontSize: font20,
);

final Icon walletIcon = Icon(
  Icons.account_balance_wallet,
  size: font14,
  color: colorMain,
);

final Icon cancelIcon = Icon(
  Icons.cancel,
  size: font14,
  color: colorMain,
);

final Icon nextIcon = Icon(
  Icons.arrow_forward_ios,
  size: font14,
  color: colorMain,
);

final Icon previousIcon = Icon(
  Icons.arrow_back_ios,
  size: font14,
  color: colorMain,
);

final Icon infoIconInactive = Icon(
  Icons.info_outline_rounded,
  color: colorMain,
  size: 25.0,
);

final Icon infoIconActive = Icon(
  Icons.info_outline_rounded,
  color: Colors.cyan.shade900,
  size: 25.0,
);

final Icon accountIcon = Icon(
  Icons.account_circle,
  size: font18,
  color: colorMain,
);

final Icon downloadIcon = Icon(
  Icons.download,
  size: font18,
  color: colorMain,
);

final Icon timerIcon = Icon(
  Icons.timer,
  size: font14,
  color: colorMain,
);

final TextStyle styleTextRead = styleLabel.copyWith(fontSize: font16);
final TextStyle styleTextReadWhite = styleLabel.copyWith(fontSize: font18, color: Colors.white);

final InputDecoration inputBeneficiary = InputDecoration(
  contentPadding: EdgeInsets.all(5.0),
  border: OutlineInputBorder(),
);

BoxDecoration gradientDecoration(Color colorFrom, Color colorTo, double radius) {
  return BoxDecoration(
    gradient: LinearGradient(
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter,
      colors: [colorFrom, colorTo],
    ),
    borderRadius: BorderRadius.circular(radius),
  );
}
