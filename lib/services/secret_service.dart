import 'dart:convert';

import 'package:dpatrimony/models/word_list.dart';
import 'package:flutter/services.dart';

class SecretService {
  Future<List<String>> expandSecretWords(String encodedSecret, WordList wordList) async {
    List<String> _restoredWordList = [];
    if (_restoredWordList.isEmpty) {
      Map<String, String> availableWords = await _loadWordList(wordList);
      List<String> words = encodedSecret.split(RegExp('\\s+'));

      for (String word in words) {
        String? fullWord = availableWords[word];
        if (fullWord == null) {
          _restoredWordList.clear();
          break;
        } else {
          _restoredWordList.add(fullWord);
        }
      }
    }
    return _restoredWordList;
  }

  Future<String> loadWordListFile(WordList wordList) async {
    String listName = wordList.toString().split(".").last.toLowerCase();
    return await rootBundle.loadString('assets/wordlists/$listName.txt');
  }

  Future<Map<String, String>> _loadWordList(WordList wordList) async {
    String list = await loadWordListFile(wordList);
    Map<String, String> words = {};
    LineSplitter.split(list).forEach((line) {
      if (line.trim().isNotEmpty) {
        int length = line.length > 4 ? 4 : line.length;
        String key = line.substring(0, length);
        words[key] = line;
      }
    });
    return words;
  }
}
