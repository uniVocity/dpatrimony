import 'dart:convert';

import 'package:dpatrimony/models/automatic_transfer/token.dart';
import 'package:dpatrimony/models/automatic_transfer/wallet.dart';
import 'package:dpatrimony/models/word_list.dart';
import 'package:flutter/services.dart';

class WalletService {
  Future<Wallet> connectWallet() async {
    Wallet out =
        Wallet(address: 'addr1q9fr7ve9k5e54z5y708wq2at8klzr0qtsc068ak64tys85uuyqndr5f0a2c8jyy0dfzngun44dnfj5vghzmcr3ylm08stpmxyx', name: 'Mary\'s wallet');

    Token ada = Token(description: 'Cardano', hash: '46yw41432342gh245', ticker: 'ADA');
    out.balance[ada] = '10000000';

    Token wmt = Token(description: 'World Mobile Token', hash: '345dg2w3vwer23r', ticker: 'WMT');
    out.balance[wmt] = '90000000.000001';

    await Future.delayed(Duration(seconds: 2));

    return out;
  }
}
