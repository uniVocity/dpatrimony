import 'dart:convert';
import 'dart:io';

import 'package:dpatrimony/models/beneficiary.dart';
import 'package:dpatrimony/models/secret.dart';
import 'package:dpatrimony/models/word_list.dart';

class QrDecoder {
  static String encodeJsonToQr(String json) {
    final encodedJson = utf8.encode(json);
    final zippedJson = gzip.encode(encodedJson);
    final base64json = base64.encode(zippedJson);
    return base64json;
  }

  static Beneficiary decodeQrString(String encodedQrString) {
    final decodedBase64 = base64.decode(encodedQrString);
    final decodedZip = gzip.decode(decodedBase64);
    final decodedJson = utf8.decode(decodedZip);

    Map<String, dynamic> details = jsonDecode(decodedJson);

    String beneficiaryName = details['n'];
    String secretDescription = details['t'];
    String dictionary = details['d'];
    String secretWords = details['s'];
    int wordListIndex = details['w'];

    Secret secret = Secret(description: secretDescription, dictionary: dictionary, encodedSecret: secretWords, wordList: WordList.values[wordListIndex]);

    Beneficiary out = Beneficiary(name: beneficiaryName);
    out.addSecret(secret);

    return out;
  }
}
