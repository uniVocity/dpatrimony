import 'dart:convert';

class TodoBeneficiary {
  String? name;
  String? description;
  String? dictionary;
  List<dynamic> wordList = [];
  int? w;

  TodoBeneficiary({required this.name, required this.description, required this.dictionary, required this.wordList, required this.w});

  TodoBeneficiary.fromJson(Map<String, dynamic> json)
      : this.name = json['n'],
        this.description = json['t'],
        this.dictionary = json['d'],
        this.wordList = jsonDecode(json['s'].toString()),
        this.w = json['w'];

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['n'] = this.name;
    data['t'] = this.description;
    data['d'] = this.dictionary;
    data['s'] = json.encode(this.wordList);
    data['w'] = this.w;
    return data;
  }
}
