import 'package:dpatrimony/models/automatic_transfer/automatic_transfer.dart';
import 'package:dpatrimony/models/automatic_transfer/beneficiary.dart';
import 'package:dpatrimony/models/automatic_transfer/share.dart';
import 'package:dpatrimony/models/automatic_transfer/token.dart';
import 'package:dpatrimony/widgets/components/app_button.dart';
import 'package:dpatrimony/widgets/components/cards/beneficiary_shares_card.dart';
import 'package:dpatrimony/widgets/components/inputs/name_input.dart';
import 'package:dpatrimony/widgets/components/inputs/wallet_address_input.dart';
import 'package:dpatrimony/widgets/components/screen_scaffold.dart';
import 'package:dpatrimony/widgets/components/table/table_helper.dart';
import 'package:dpatrimony/widgets/vertical_space.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:sizer/sizer.dart';

class BeneficiarySharesScreen extends StatefulWidget {
  final AutomaticTransfer transfer;
  final Beneficiary beneficiary;
  Beneficiary beneficiaryEditing = Beneficiary();
  final bool isEditing;
  final dynamic updateState;
  bool valueModification = true;

  BeneficiarySharesScreen(this.transfer, this.beneficiary, this.isEditing, this.updateState);

  @override
  _BeneficiarySharesScreenState createState() => _BeneficiarySharesScreenState();
}

class _BeneficiarySharesScreenState extends State<BeneficiarySharesScreen> {
  void updateState() {
    setState(() {});
  }

  void addShare(String tokenValue, String shareTypeValue, double numberValue) {
    Share share = Share();
    switch (shareTypeValue) {
      case 'Percentage':
        share.shareType = ShareType.Percentage;
        break;
      case 'Remainder':
        share.shareType = ShareType.Remainder;
        break;
      case 'Fixed':
        share.shareType = ShareType.FixedAmount;
        break;
    }
    share.amount = numberValue;

    String tokenDescription = '';
    switch (tokenValue) {
      case 'ADA':
        tokenDescription = 'Cardano';
        break;
      case 'WMT':
        tokenDescription = 'World mobile Token';
        break;
      case 'ALL':
        tokenDescription = 'All coins';
    }
    Token token = Token(description: tokenDescription, hash: tokenValue.hashCode.toString(), ticker: tokenValue);

    setState(() {
      widget.beneficiaryEditing.addShare(token, share);
    });
  }

  void changeValueModificationToFalse(){
    setState(() {
      widget.valueModification = false;
    });
  }

  @override
  Widget build(BuildContext context) {

    if(widget.isEditing && widget.valueModification){
      widget.beneficiaryEditing.name = widget.beneficiary.name;
      widget.beneficiaryEditing.receivingWalletAddress = widget.beneficiary.receivingWalletAddress;
      widget.beneficiary.shares.forEach((key, value) {
        widget.beneficiaryEditing.addShare(key, value);
      });
    }

    Table functionRow = TableHelper.addFunctionRow(
      'add share',
      () {
        showModalBottomSheet(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.vertical(top: Radius.circular(10.0))),
          context: context,
          isScrollControlled: true,
          builder: (context) => SingleChildScrollView(
            padding: EdgeInsets.only(
              bottom: MediaQuery.of(context).viewInsets.bottom,
            ),
            child: BeneficiarySharesCard(addShare, updateState, widget.beneficiaryEditing),
          ),
        );
      },
    );

    List<Widget> headerAndBody = TableHelper.getTableHeaderAndBody(getShareTableRows(), functionRow, 'No shares defined yet');

    dynamic errorOnSaving({required bool boolName, required bool boolReceivingAddress, required bool boolShares}) {
      List<Widget> textsErrors = [];

      void addTextError(String text) {
        textsErrors.add(
          Text(
            text,
            textAlign: TextAlign.center,
          ),
        );
      }

      if (boolName) addTextError('Name Input is empty');
      if (boolReceivingAddress) addTextError('Wallet Address is empty');
      if (boolShares) addTextError('Share is empty');

      return showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(20.0))),
          content: Container(
            height: 180.0,
            width: 50.0,
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.only(bottom: 10.0),
                    child: Text(
                      'Error on Saving',
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.red, fontSize: 25, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Column(
                    children: textsErrors,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10.0),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(primary: Colors.black54),
                      onPressed: () => Navigator.pop(context, 'OK'),
                      child: Text('Ok'),
                    ),
                  ),
                ],
              ),
            ),
          ),
          contentPadding: EdgeInsets.only(top: 5.0, bottom: 5.0),
        ),
      );
    }

    List<Widget> appButtons = widget.isEditing
        ? [
            AppButton.full(
                label: 'Save',
                onPressed: () {
                  if (widget.beneficiaryEditing.name.trim().isNotEmpty &&
                      widget.beneficiaryEditing.receivingWalletAddress.trim().isNotEmpty &&
                      widget.beneficiaryEditing.shareDescription.trim().isNotEmpty) {
                    widget.transfer.removeBeneficiary(widget.beneficiary);
                    widget.transfer.addBeneficiary(widget.beneficiaryEditing);
                    Navigator.pop(context);
                  } else {
                    errorOnSaving(
                        boolName: widget.beneficiaryEditing.name.trim().isEmpty,
                        boolReceivingAddress: widget.beneficiaryEditing.receivingWalletAddress.trim().isEmpty,
                        boolShares: widget.beneficiaryEditing.shareDescription.trim().isEmpty);
                  }
                  widget.updateState();
                }),
            AppButton.red(
                label: 'Remove',
                onPressed: () {
                  widget.transfer.removeBeneficiary(widget.beneficiary);
                  widget.updateState();
                  Navigator.pop(context);
                }),
          ]
        : [
            AppButton.full(
                label: 'Save',
                onPressed: () {
                  String _shareDescriptionEditing = widget.beneficiaryEditing.shareDescription;
                  if (widget.beneficiaryEditing.name.trim().isNotEmpty &&
                      widget.beneficiaryEditing.receivingWalletAddress.trim().isNotEmpty &&
                      _shareDescriptionEditing.trim().isNotEmpty) {
                    widget.transfer.addBeneficiary(widget.beneficiaryEditing);
                    widget.updateState();
                    Navigator.pop(context);
                  } else {
                    errorOnSaving(
                        boolName: widget.beneficiaryEditing.name.trim().isEmpty,
                        boolReceivingAddress: widget.beneficiaryEditing.receivingWalletAddress.trim().isEmpty,
                        boolShares: _shareDescriptionEditing.trim().isEmpty);
                  }
                }),
          ];

    return Container(
      decoration: BoxDecoration(),
      child: ScreenScaffold(
        appBarTitle: 'Beneficiary shares',
        screenTitle: 'Add beneficiary',
        children: [
          VerticalSpace(),
          NameInput(widget.beneficiaryEditing, (v) {
            widget.beneficiaryEditing.name = v.trim();
          }),
          VerticalSpace(),
          WalletAddressInput(widget.beneficiaryEditing, (v) {
            widget.beneficiaryEditing.receivingWalletAddress = v.trim();
          }),
          VerticalSpace(),
          headerAndBody[0],
          headerAndBody[1],
          VerticalSpace(),
          SvgPicture.asset(
            'assets/images/wallet_balance_background.svg',
            height: 20.h,
          ),
          Column(
            children: appButtons,
          ),
        ],
      ),
    );
  }

  List<Map<String, IconData?>> shareTableRows() {
    String shareDescription = widget.beneficiaryEditing.shareDescription;
    List<Map<String, IconData?>> shareTable = [];
    List<String> shareList = shareDescription.split(", ");
    if (shareDescription.isNotEmpty) {
      for (int i = 0; i < shareList.length; i++) {
        List<String> shareInfo = shareList[i].split(' ');
        shareTable.add({'${shareInfo[1]}': null, '${shareInfo[0]}': null});
      }
    }

    return shareTable;
  }

  List<TableRow> getShareTableRows() {
    return TableHelper.rows(
        hasAddFunction: true,
        onTap: (row) {
          showModalBottomSheet(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.vertical(top: Radius.circular(10.0))),
            context: context,
            isScrollControlled: true,
            builder: (context) => SingleChildScrollView(
              padding: EdgeInsets.only(
                bottom: MediaQuery.of(context).viewInsets.bottom,
              ),
              child: BeneficiarySharesCard(addShare, updateState, widget.beneficiaryEditing, row - 1, changeValueModificationToFalse),
            ),
          );
        },
        headers: {
          'Token': Icons.attach_money_outlined,
          'Share': Icons.description_outlined,
        },
        rows: shareTableRows());
  }
}
