import 'package:dpatrimony/constants/constants.dart';
import 'package:dpatrimony/models/automatic_transfer/automatic_transfer.dart';
import 'package:dpatrimony/models/automatic_transfer/beneficiary.dart';
import 'package:dpatrimony/models/automatic_transfer/period.dart';
import 'package:dpatrimony/models/automatic_transfer/wallet.dart';
import 'package:dpatrimony/screens/automatic_transfer/beneficiary_shares_screen.dart';
import 'package:dpatrimony/widgets/components/app_button.dart';
import 'package:dpatrimony/widgets/components/inputs/number_input.dart';
import 'package:dpatrimony/widgets/components/labels/card_decorated_icon.dart';
import 'package:dpatrimony/widgets/components/labels/card_decorated_label.dart';
import 'package:dpatrimony/widgets/components/screen_scaffold.dart';
import 'package:dpatrimony/widgets/components/table/table_helper.dart';
import 'package:dpatrimony/widgets/vertical_space.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:sizer/sizer.dart';

class BeneficiaryDetailsScreen extends StatefulWidget {
  final AutomaticTransfer transfer;

  BeneficiaryDetailsScreen(this.transfer);

  @override
  _BeneficiaryDetailsScreenState createState() => _BeneficiaryDetailsScreenState();
}

class _BeneficiaryDetailsScreenState extends State<BeneficiaryDetailsScreen> {
  int choice = 1;

  FocusNode _focus = new FocusNode();
  TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    _focus.addListener(_onFocusChange);
  }

  void _onFocusChange() {
    if (!_focus.hasFocus) {
      Navigator.pop(context);
    }
  }

  void updateState() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    AutomaticTransfer transfer = widget.transfer;
    Wallet wallet = transfer.wallet;

    Table functionRow = TableHelper.addFunctionRow(
      'new beneficiary',
      () {
        Beneficiary beneficiary = Beneficiary();
        Navigator.push(context, MaterialPageRoute(builder: (context) => BeneficiarySharesScreen(transfer, beneficiary, false, updateState)));
      },
    );

    List<Widget> headerAndBody = TableHelper.getTableHeaderAndBody(
        wallet.displayingBalances ? getWalletBalanceTableRows() : getBeneficiaryTableRows(), functionRow, 'No beneficiaries defined yet');

    return ScreenScaffold(
      appBarTitle: 'Automatic transfer setup',
      screenTitle: 'Beneficiaries',
      children: [
        VerticalSpace(),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              child: Container(
                height: 7.h,
                decoration: new BoxDecoration(
                  color: Color(0xFF1F232C).withOpacity(0.08),
                  borderRadius: new BorderRadius.all(Radius.circular(10.0)),
                ),
                child: CardDecoratedLabel.actionable(
                  label: wallet.name,
                  details: wallet.address,
                  onTap: () {
                    _controller.text = wallet.name;
                    _controller.selection = TextSelection(baseOffset: wallet.name.length, extentOffset: wallet.name.length);

                    showModalBottomSheet(
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.vertical(top: Radius.circular(10.0))),
                      context: context,
                      isScrollControlled: true,
                      builder: (context) => WillPopScope(
                        onWillPop: () async => false,
                        child: SingleChildScrollView(
                          padding: EdgeInsets.only(
                            bottom: MediaQuery.of(context).viewInsets.bottom,
                          ),
                          child: TextField(
                            controller: _controller,
                            focusNode: _focus,
                            onChanged: (string) {
                              setState(() {
                                wallet.name = string;
                              });
                            },
                            autofocus: true,
                            keyboardType: TextInputType.name,
                          ),
                        ),
                      ),
                    );
                  },
                  icon: CardDecoratedIcon(
                    Icons.account_balance_wallet_outlined,
                    pressed: wallet.displayingBalances,
                    onTap: () {
                      setState(() {
                        wallet.toggleDisplayingBalances();
                      });
                    },
                  ),
                  iconPadding: EdgeInsets.symmetric(vertical: 1.2.h, horizontal: 2.w),
                ),
              ),
            ),
          ],
        ),
        VerticalSpace(),
        headerAndBody[0],
        headerAndBody[1],
        VerticalSpace(),
        wallet.displayingBalances
            ? SvgPicture.asset(
                'assets/images/wallet_balance_background.svg',
                height: 22.h,
              )
            : TransferTimer(),
      ],
    );
  }

  List<TableRow> getBeneficiaryTableRows() {
    List<Map<String, IconData?>> rows = [];
    for (Beneficiary beneficiary in widget.transfer.beneficiaries) {
      rows.add(
        {
          beneficiary.name: null,
          beneficiary.shareDescription: null,
        },
      );
    }

    return TableHelper.rows(
      hasAddFunction: true,
      onTap: (row) {
        Beneficiary beneficiary = widget.transfer.beneficiaries[row - 1];
        Navigator.push(context, MaterialPageRoute(builder: (context) => BeneficiarySharesScreen(widget.transfer, beneficiary, true, updateState)));
      },
      headers: {
        'Beneficiaries': Icons.account_circle_outlined,
        'Shares': Icons.description_outlined,
      },
      rows: rows,
    );
  }

  List<TableRow> getWalletBalanceTableRows() {
    return TableHelper.rows(
      hasAddFunction: true,
      onTap: (row) {},
      headers: {
        'Token': Icons.account_balance_outlined,
        'Balance': Icons.attach_money_outlined,
      },
      rows: [
        {
          'ADA': null,
          '4000.000000': null,
        },
        {
          'WMT': null,
          '23451.00037323': null,
        },
        {
          'AGIX': null,
          '644333.31': null,
        },
      ],
    );
  }
}

extension InactivityPeriod on Period {
  static List<DropdownMenuItem<Period>> items() {
    List<DropdownMenuItem<Period>> out = [];
    for (Period period in Period.values) {
      out.add(DropdownMenuItem(child: Text(period.toString().split(".").last), value: period));
    }
    return out;
  }
}

class TransferTimer extends StatefulWidget {
  @override
  State<TransferTimer> createState() => _TransferTimerState();
}

class _TransferTimerState extends State<TransferTimer> {
  Period period = Period.months;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Text(
          'Send funds after',
          style: styleLabel,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            NumberInput(),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
              child: SizedBox(
                height: 6.h,
                width: 35.w,
                child: InputDecorator(
                  decoration: kTextFieldDecoration,
                  child: DropdownButton<dynamic>(
                    style: styleValue,
                    underline: SizedBox.shrink(),
                    isDense: true,
                    isExpanded: true,
                    items: InactivityPeriod.items(),
                    value: period,
                    onChanged: (v) {
                      setState(() {
                        period = v;
                      });
                    },
                  ),
                ),
              ),
            ),
            Text(
              'of inactivity',
              style: styleLabel,
            ),
          ],
        ),
        AppButton.full(
          label: 'Activate',
          icon: Icons.save_outlined,
          onPressed: () {},
        ),
      ],
    );
  }
}
