import 'package:dpatrimony/main.dart';
import 'package:dpatrimony/widgets/components/labels/screen_action_text.dart';
import 'package:dpatrimony/widgets/components/table/table_helper.dart';
import 'package:flutter/material.dart';
import 'package:dpatrimony/constants/constants.dart';

class ConnectedWalletsScreen extends StatefulWidget {
  static const String id = 'connected_wallets_screen';

  @override
  _ConnectedWalletsScreenState createState() => _ConnectedWalletsScreenState();
}

class _ConnectedWalletsScreenState extends State<ConnectedWalletsScreen> {
  final List<Map<String, IconData?>> teste = [
    {
      'wallet1': null,
      '4': null,
      'Inactive': null,
    },
    {
      'wallet1': null,
      '4': null,
      '90 days left': null,
    },
    {
      'wallet1': null,
      '4': null,
      'Sent': null,
    },
    {
      'wallet1': null,
      '2': null,
      'Cancelled': null,
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Connected Wallets'),
      ),
      body: Container(
        padding: EdgeInsets.all(5.0),
        child: Column(
          children: <Widget>[
            ScreenActionText('Automatic transfer Setup'),
            Row(
              children: <Widget>[
                Text('Connected Wallets: ', style: styleCardTitle),
              ],
            ),
            SizedBox(height: 50.0),
            Container(
              child: Table(
                children: TableHelper.rows(
                    hasAddFunction: true,
                    onTap: (row) {
                      showModalBottomSheet(
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.vertical(top: Radius.circular(10.0))),
                        context: context,
                        isScrollControlled: true,
                        builder: (context) => SingleChildScrollView(
                          padding: EdgeInsets.only(
                            bottom: MediaQuery.of(context).viewInsets.bottom,
                          ),
                          child: Container(
                            child: Column(
                              children: [Text('TODO')],
                            ),
                          ),
                        ),
                      );
                    },
                    headers: {
                      'Name': null,
                      'Beneficiaries': null,
                      'Status': null,
                    },
                    rows: teste),
              ),
            ),
            Expanded(child: SizedBox()),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                ElevatedButton.icon(
                  onPressed: () {},
                  label: Text('Connect another wallet'),
                  icon: walletIcon,
                ),
                Expanded(
                  child: SizedBox(),
                ),
                ElevatedButton.icon(
                  onPressed: () => MyApp.goBackHome(context),
                  label: Text('Home'),
                  icon: homeIcon,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
