import 'package:dpatrimony/constants/constants.dart';
import 'package:dpatrimony/models/automatic_transfer/automatic_transfer.dart';
import 'package:dpatrimony/models/automatic_transfer/automatic_transfers.dart';
import 'package:dpatrimony/models/automatic_transfer/wallet.dart';
import 'package:dpatrimony/screens/automatic_transfer/beneficiary_details_screen.dart';
import 'package:dpatrimony/services/wallet_service.dart';
import 'package:dpatrimony/widgets/components/app_button.dart';
import 'package:dpatrimony/widgets/components/screen_scaffold.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ConnectWalletScreen extends StatefulWidget {
  static const String id = 'connect_wallet_screen';

  @override
  _ConnectWalletScreenState createState() => _ConnectWalletScreenState();
}

class _ConnectWalletScreenState extends State<ConnectWalletScreen> {
  @override
  Widget build(BuildContext context) {
    return ScreenScaffold(
      appBarTitle: 'Automatic transfer setup',
      screenTitle: 'Connect wallet',
      children: [
        SvgPicture.asset('assets/images/wallet.svg'),
        Text(
          'Automatic transfers are available only for Cardano Wallets.\n\nPlease press the "connect wallet" button below to proceed',
          textAlign: TextAlign.center,
          style: h4Style,
        ),
        AppButton.outlined(
            label: 'Connect wallet',
            icon: Icons.account_balance_wallet_outlined,
            onPressed: () {
              connectWallet();
            }),
      ],
    );
  }

  void connectWallet() async {
    try {
      EasyLoading.show(status: 'Connecting...');
      WalletService walletService = WalletService();
      Wallet wallet = await walletService.connectWallet();

      AutomaticTransfer transfer = AutomaticTransfers.instance.getOrCreateAutomaticTransfer(wallet);

      Navigator.push(context, MaterialPageRoute(builder: (context) => BeneficiaryDetailsScreen(transfer)));
    } finally {
      EasyLoading.dismiss();
    }
  }
}
