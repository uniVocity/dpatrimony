import 'dart:convert';

import 'package:dpatrimony/constants/constants.dart';
import 'package:dpatrimony/models/beneficiary.dart';
import 'package:dpatrimony/services/todo_beneficiary.dart';
import 'package:dpatrimony/widgets/components/app_card.dart';
import 'package:dpatrimony/widgets/components/labels/card_title.dart';
import 'package:dpatrimony/widgets/components/secret/secret_word_list.dart';
import 'package:dpatrimony/models/secret.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class SecretDetails extends StatefulWidget {
  final String? title;
  final Beneficiary beneficiary;
  int? numberSecret;

  SecretDetails({this.title, required this.beneficiary, this.numberSecret});

  @override
  State<SecretDetails> createState() => _SecretDetailsState();
}

class _SecretDetailsState extends State<SecretDetails> {
  @override
  Widget build(BuildContext context) {
    final String beneficiaryName = widget.beneficiary.name;
    final String dictionary = widget.numberSecret == null ? widget.beneficiary.secrets.first.dictionary : widget.beneficiary.secrets.elementAt(widget.numberSecret!).dictionary;
    final String description = widget.numberSecret == null ? widget.beneficiary.secrets.first.description : widget.beneficiary.secrets.elementAt(widget.numberSecret!).description;
    Future<List<String>> secrets = widget.numberSecret == null ? widget.beneficiary.secrets.first.getWordList() : widget.beneficiary.secrets.elementAt(widget.numberSecret!).getWordList();

    return FutureBuilder<List<String>>(
      future: secrets,
      builder: (context, AsyncSnapshot<List<String>> snapshot) {
        if (!snapshot.hasData) {
          return CircularProgressIndicator();
        }
        final List<String> wordList = snapshot.data!;
        return Container(
          child: Column(
            children: [
              widget.title != null ? CardTitle('Secret details') : SizedBox(height: 0),
              AppCard(
                height: 6.h,
                child: Stack(
                  children: [
                    Center(
                      child: Text(
                        '$beneficiaryName',
                        style: h4Style,
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Positioned(
                      right: 1,
                      height: 6.h,
                      child: Padding(
                        padding: EdgeInsets.only(right: 15),
                        child: Row(
                          children: [
                            Text(
                              '$dictionary',
                              style: h4Style.copyWith(color: Color(0xFF707583)),
                            ),
                            Icon(
                              Icons.book_outlined,
                              size: font16,
                              color: Color(0xFF707583),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 15),
              SecretWordList(description: description, wordList: wordList),
            ],
          ),
        );
      },
    );
  }
}
