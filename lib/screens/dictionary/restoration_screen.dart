import 'dart:convert';

import 'package:dpatrimony/constants/constants.dart';
import 'package:dpatrimony/models/beneficiary.dart';
import 'package:dpatrimony/services/todo_beneficiary.dart';
import 'package:dpatrimony/widgets/components/app_button.dart';
import 'package:dpatrimony/widgets/components/labels/screen_action_text.dart';
import 'package:dpatrimony/widgets/components/screen_scaffold.dart';
import 'package:dpatrimony/widgets/components/secret/secret_word_list.dart';
import 'package:flutter/material.dart';

import '../../main.dart';

class RestorationScreen extends StatelessWidget {
  static const String id = 'restoration_screen';

  @override
  Widget build(BuildContext context) {
    final route = ModalRoute.of(context);
    if (route == null) return SizedBox.shrink();
    final routeArgs = route.settings.arguments as Beneficiary;

    Beneficiary beneficiary = routeArgs;

    final String description = beneficiary.secrets.first.description;
    final String dictionary = beneficiary.secrets.first.dictionary;
    final String beneficiaryName = beneficiary.name;

    return FutureBuilder<List<String>>(
      future: beneficiary.secrets.first.getWordList(),
      builder: (context, AsyncSnapshot<List<String>> snapshot) {
        final List<String> wordList = snapshot.data!;
        if (!snapshot.hasData) {
          return CircularProgressIndicator();
        } else {
          return ScreenScaffold(
            appBarTitle: 'Restoration',
            screenTitle: 'Restore your secret',
            children: [
              Expanded(
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        'Please load the "$dictionary" dictionary on dPatrimony in your computer, then input the secret words below to reveal the $description access keys',
                        textAlign: TextAlign.center,
                        style: h4Style,
                      ),
                      SecretWordList(description: description, wordList: wordList),
                      Text(
                        'Write down the decoded access keys to a piece of paper and make sure your computer is not connected to the internet',
                        textAlign: TextAlign.center,
                        style: h4Style,
                      ),
                    ],
                  ),
                ),
              ),
              AppButton.full(
                onPressed: () => MyApp.goBackHome(context),
                label: 'Home',
                icon: Icons.home_outlined,
              ),
            ],
          );
        }
      },
    );
  }
}
