import 'package:dpatrimony/constants/constants.dart';
import 'package:dpatrimony/widgets/components/inputs/payment_list.dart';
import 'package:dpatrimony/widgets/components/labels/screen_action_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ActivationScreen extends StatefulWidget {
  static const String id = 'activation_screen';

  @override
  _ActivationScreenState createState() => _ActivationScreenState();
}

class _ActivationScreenState extends State<ActivationScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Activation'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              ScreenActionText('Select payment method'),
              Column(
                children: [
                  Text('Activating enables the app to submit all encoded secrets to your beneficiaries and save your changes anonymously on the cloud.'),
                  SizedBox(height: 50),
                  PaymentList(),
                  SizedBox(height: 50),
                  ElevatedButton.icon(
                    onPressed: () {},
                    label: Text('Activate'),
                    icon: payIcon,
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
