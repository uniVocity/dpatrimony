import 'dart:convert';

import 'package:dpatrimony/constants/constants.dart';
import 'package:dpatrimony/screens/beneficiaryqrcode.dart';
import 'package:dpatrimony/screens/dictionary/secret_scanning_screen.dart';
import 'package:dpatrimony/widgets/components/app_button.dart';
import 'package:dpatrimony/widgets/components/screen_scaffold.dart';
import 'package:dpatrimony/models/app_data.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:sizer/sizer.dart';

const _url = 'https://dpatrimony.com/download';

class DownloadAppScreen extends StatefulWidget {
  static const String id_encode = 'download_app_screen_encode';
  static const String id_restore = 'download_app_screen_restore';
  final bool restoring;

  DownloadAppScreen(this.restoring);

  DownloadAppScreen.encoding() : this(false);
  DownloadAppScreen.restoring() : this(true);

  @override
  _DownloadAppScreenState createState() => _DownloadAppScreenState();
}

class _DownloadAppScreenState extends State<DownloadAppScreen> {
  @override
  Widget build(BuildContext context) {
    String description;
    if (widget.restoring) {
      description = ' and run the dPatrimony app on a computer to decode your secrets.';
    } else {
      description = ' and run the dPatrimony app on a computer to encode your secrets, then import them here.';
    }

    return ScreenScaffold(
      appBarTitle: widget.restoring ? 'Restoration setup' : 'Dictionary setup',
      screenTitle: 'Download dPatrimony',
      children: [
        ElevatedButton.icon(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => BeneficiaryQrCode(),
                ),
              );
            },
            icon: Icon(Icons.qr_code),
            label: Text('QrCode')),
        SizedBox(height: 80),
        SvgPicture.asset(
          'assets/images/download.svg',
          height: 25.h,
        ),
        RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            style: h3Style,
            children: <TextSpan>[
              TextSpan(text: 'Please '),
              TextSpan(
                  text: 'download',
                  style: linkStyle,
                  recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      _launchInBrowser(_url);
                    }),
              TextSpan(text: description),
            ],
          ),
        ),
        AppButton.outlined(
            icon: Icons.arrow_circle_down_outlined,
            label: 'Go to download page',
            onPressed: () {
              _launchInBrowser(_url);
            }),
        SizedBox(),
        SizedBox(),
        RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            style: h3Style,
            children: <TextSpan>[
              TextSpan(text: 'Once you are done\ntap '),
              TextSpan(text: 'import', style: linkStyle, recognizer: TapGestureRecognizer()..onTap = () => onImport()),
              TextSpan(text: ' to proceed.'),
            ],
          ),
        ),
        AppButton.full(icon: Icons.download, label: 'Import', onPressed: () => onImport()),
      ],
    );
  }

  Future<void> onImport() async {
    if (widget.restoring) {
      Navigator.pushNamed(context, SecretScanningScreen.id_restore);
    } else {
      AppData storageBeneficiaries = AppData(beneficiaries: {});
      Navigator.pushNamed(context, SecretScanningScreen.id_encode, arguments: storageBeneficiaries.readAll());
    }
  }

  Future<void> _launchInBrowser(String url) async {
    if (await canLaunch(url)) {
      await launch(
        url,
        forceSafariVC: false,
        forceWebView: false,
      );
    } else {
      throw 'Could not launch $url';
    }
  }
}

//
//
