import 'package:dpatrimony/main.dart';
import 'package:dpatrimony/models/app_data.dart';
import 'package:dpatrimony/models/beneficiary.dart';
import 'package:dpatrimony/screens/dictionary/activation_screen.dart';
import 'package:dpatrimony/screens/dictionary/secret_scanning_screen.dart';
import 'package:dpatrimony/widgets/button_vertical_space.dart';
import 'package:dpatrimony/widgets/components/app_button.dart';
import 'package:dpatrimony/widgets/components/cards/details_card.dart';
import 'package:dpatrimony/widgets/components/inputs/email_input.dart';
import 'package:dpatrimony/widgets/components/inputs/message_input.dart';
import 'package:dpatrimony/widgets/components/inputs/wallet_list.dart';
import 'package:dpatrimony/widgets/components/labels/card_decorated_icon.dart';
import 'package:dpatrimony/widgets/components/labels/card_decorated_label.dart';
import 'package:dpatrimony/widgets/components/labels/screen_action_text.dart';
import 'package:dpatrimony/widgets/components/screen_scaffold.dart';
import 'package:dpatrimony/widgets/components/table/table_helper.dart';
import 'package:dpatrimony/widgets/vertical_space.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';

import 'secret_details.dart';

class SharedSecretScreen extends StatefulWidget {
  static const String id = 'shared_secret_screen';

  Map<String, dynamic> arguments;

  SharedSecretScreen(this.arguments);

  bool displaySecret = false;

  @override
  _SharedSecretScreenState createState() => _SharedSecretScreenState();
}

class _SharedSecretScreenState extends State<SharedSecretScreen> {
  @override
  Widget build(BuildContext context) {
    // code to refresh the screen every 10 seconds
    Future.delayed(Duration(seconds: 10), () {
      setState(() {});
    });

    AppData storageBeneficiaries = AppData(beneficiaries: widget.arguments);

    List<Map<String, IconData?>> createTableBeneficiaries(Map<String, dynamic> listBeneficiaries) {
      AppData beneficiaries = AppData(beneficiaries: listBeneficiaries);

      Map<String, IconData?> tableBeneficiaries = {};
      List<Map<String, IconData?>> returnData = [];

      for (int i = 0; i < beneficiaries.getLength(); i++) {
        Beneficiary beneficiary = beneficiaries.readOnly(beneficiaries.readSpecificKey(i));
        List<String> description = [];
        for (int i = 0; i <= beneficiary.secrets.length - 1; i++) {
          description.add(beneficiary.secrets.elementAt(i).description);
        }
        tableBeneficiaries = {
          beneficiary.name.toString(): null,
          description.join(', '): null,
          '': beneficiary.isEmailConfirmed() ? Icons.verified : Icons.cancel,
        };
        returnData.add(tableBeneficiaries);
      }
      return returnData;
    }

    Table functionRow = TableHelper.addFunctionRow('import new secret', () {
      Navigator.pushNamed(context, SecretScanningScreen.id_encode, arguments: storageBeneficiaries.readAll());
    });

    List<Widget> headerAndBody = TableHelper.getTableHeaderAndBody(
        getBeneficiaryTableRows(createTableBeneficiaries(storageBeneficiaries.readAll()), storageBeneficiaries), functionRow, 'No secrets imported yet');

    return ScreenScaffold(
      appBarTitle: 'Beneficiaries',
      screenTitle: 'Secret management',
      checkInternet: true,
      children: [
        ScreenActionText('Tap each beneficiary to finalize your setup'),
        headerAndBody[0],
        headerAndBody[1],
        VerticalSpace(),
        Container(
          child: Column(
            children: [
              AppButton.full(
                icon: Icons.save_outlined,
                label: 'Activate',
                onPressed: () {
                  Navigator.pushNamed(context, ActivationScreen.id);
                },
              ),
              ButtonVerticalSpace(),
              AppButton.black(
                icon: Icons.home_outlined,
                label: 'Home',
                onPressed: () => MyApp.goBackHome(context),
              ),
            ],
          ),
        ),
      ],
    );
  }

  List<TableRow> getBeneficiaryTableRows(List<Map<String, IconData?>> beneficiariesRows, AppData storageBeneficiaries) {
    return TableHelper.rows(
      hasAddFunction: true,
      onTap: (row) {
        showModalBottomSheet(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.vertical(top: Radius.circular(10.0))),
          context: context,
          isScrollControlled: true,
          builder: (context) => SingleChildScrollView(
            padding: EdgeInsets.only(
              bottom: MediaQuery.of(context).viewInsets.bottom,
            ),
            child: Container(
              child: DialogContent(widget, storageBeneficiaries.readOnly(storageBeneficiaries.readSpecificKey(row - 1))),
            ),
          ),
        ).whenComplete(() {
          setState(() {});
        });
      },
      headers: {
        'Beneficiary': Icons.account_circle_outlined,
        'Secrets': Icons.vpn_key_outlined,
        'Status': Icons.info_outline,
      },
      rows: beneficiariesRows,
    );
  }
}

class DialogContent extends StatefulWidget {
  final SharedSecretScreen parent;
  final Beneficiary beneficiary;

  DialogContent(this.parent, this.beneficiary);

  @override
  _DialogContentState createState() => _DialogContentState();
}

class _DialogContentState extends State<DialogContent> {
  @override
  Widget build(BuildContext context) {
    return widget.parent.displaySecret
        ? SecretDetailsCard(() {
            setState(() {
              widget.parent.displaySecret = false;
            });
          }, widget.beneficiary)
        : BeneficiaryDetailsCard(() {
            setState(() {
              widget.parent.displaySecret = true;
            });
          }, widget.beneficiary);
  }
}

class BeneficiaryDetailsCard extends StatefulWidget {
  final VoidCallback onTap;
  Beneficiary beneficiary;

  BeneficiaryDetailsCard(this.onTap, this.beneficiary);

  @override
  State<BeneficiaryDetailsCard> createState() => _BeneficiaryDetailsCardState();
}

class _BeneficiaryDetailsCardState extends State<BeneficiaryDetailsCard> {
  @override
  Widget build(BuildContext context) {

    EmailInput inputEmail = EmailInput(beneficiaryData: widget.beneficiary);
    MessageInput inputMessage = MessageInput(beneficiaryData: widget.beneficiary,);

    return DetailsCard(
      children: [
        CardDecoratedLabel(
          '${widget.beneficiary.name}',
          CardDecoratedIcon.raised(Icons.vpn_key_outlined, widget.onTap),
        ),
        VerticalSpace(),
        inputEmail,
        VerticalSpace(),
        inputMessage,
        VerticalSpace(),
        AppButton.full(
          label: 'Save',
          onPressed: () {
            if ((EmailValidator.validate(inputEmail.email.toString()))) {
              widget.beneficiary.setMessage(inputMessage.message);
              widget.beneficiary.setEmail(inputEmail.email);
              Navigator.pop(context);
            }
          },
        ),
      ],
    );
  }
}

class SecretDetailsCard extends StatefulWidget {
  final VoidCallback onTap;
  final Beneficiary beneficiary;

  SecretDetailsCard(this.onTap, this.beneficiary);

  int numberSharedSecret = 0;

  @override
  State<SecretDetailsCard> createState() => _SecretDetailsCardState();
}

class _SecretDetailsCardState extends State<SecretDetailsCard> {
  @override
  Widget build(BuildContext context) {
    void changeNumberSharedSecret(int number) {
      setState(() {
        widget.numberSharedSecret = number;
      });
    }

    WalletList walletList = WalletList(widget.beneficiary, widget.numberSharedSecret, changeNumberSharedSecret);
    SecretDetails detailsSecret = SecretDetails(
      beneficiary: widget.beneficiary,
      numberSecret: widget.numberSharedSecret,
    );

    return DetailsCard(
      children: [
        CardDecoratedLabel(
          '${widget.beneficiary.name}',
          CardDecoratedIcon.pressed(Icons.vpn_key_outlined, widget.onTap),
        ),
        VerticalSpace(),
        walletList,
        VerticalSpace(),
        detailsSecret,
      ],
    );
  }
}
