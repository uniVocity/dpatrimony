import 'dart:convert';

import 'package:dpatrimony/models/beneficiary.dart';
import 'package:dpatrimony/screens/dictionary/restoration_screen.dart';
import 'package:dpatrimony/screens/dictionary/secret_details.dart';
import 'package:dpatrimony/screens/dictionary/shared_secret_screen.dart';
import 'package:dpatrimony/services/todo_beneficiary.dart';
import 'package:dpatrimony/widgets/button_vertical_space.dart';
import 'package:dpatrimony/widgets/components/app_button.dart';
import 'package:dpatrimony/widgets/components/screen_scaffold.dart';
import 'package:dpatrimony/models/app_data.dart';
import 'package:flutter/material.dart';

class SecretDetailsScreen extends StatefulWidget {
  static const String id_encode = 'secret_details_screen_encode';
  static const String id_restore = 'secret_details_screen_restore';

  final bool restoring;

  @override
  _SecretDetailsScreen createState() => _SecretDetailsScreen();

  SecretDetailsScreen(this.restoring);

  SecretDetailsScreen.encoding() : this(false);
  SecretDetailsScreen.restoring() : this(true);
}

class _SecretDetailsScreen extends State<SecretDetailsScreen> {
  @override
  Widget build(BuildContext context) {
    //extract the arguments

    dynamic routeArgs() {
      final route = ModalRoute.of(context);
      if (route == null) return SizedBox.shrink();
      if(widget.restoring)
        return route.settings.arguments as Beneficiary;
      else
        return route.settings.arguments as List;
    }

    dynamic getBeneficiary() {
      if (widget.restoring) {
        return routeArgs();
      } else {
        final List<dynamic> storageData = routeArgs();

        return storageData[1];
      }
    }

    final VoidCallback onPressed = () {
      if (widget.restoring) {
        Navigator.popAndPushNamed(context, RestorationScreen.id, arguments: routeArgs());
      } else {
        final List<dynamic> storageData = routeArgs();

        Beneficiary beneficiary = storageData[1];
        AppData storageBeneficiaries = AppData(beneficiaries: storageData[0]);

        if (storageBeneficiaries.readOnly(beneficiary.name) == null) {
          storageBeneficiaries.add(beneficiary);
        } else {
          Beneficiary beneficiaryTemp = storageBeneficiaries.readOnly(beneficiary.name);
          beneficiaryTemp.addSecret(beneficiary.secrets.first);
          storageBeneficiaries.add(beneficiaryTemp);
        }

        Navigator.push(context, MaterialPageRoute(builder: (context) => SharedSecretScreen(storageBeneficiaries.readAll())));
      }
    };

    return ScreenScaffold(
      appBarTitle: 'Review',
      screenTitle: 'Secret details',
      children: [
        SizedBox(height: 80),
        Expanded(
          child: SecretDetails(
            beneficiary: getBeneficiary(),
          ),
        ),
        Container(
          child: Column(
            children: [
              AppButton.full(
                icon: Icons.check,
                label: 'Accept',
                onPressed: onPressed,
              ),
              ButtonVerticalSpace(),
              AppButton.red(
                icon: Icons.clear,
                label: 'Discard',
                onPressed: onPressed,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
