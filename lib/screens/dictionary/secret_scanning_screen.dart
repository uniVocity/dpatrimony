import 'dart:convert';

import 'package:dpatrimony/constants/constants.dart';
import 'package:dpatrimony/screens/dictionary/restoration_screen.dart';
import 'package:dpatrimony/screens/dictionary/secret_details_screen.dart';
import 'package:dpatrimony/services/qr_decoder.dart';
import 'package:dpatrimony/services/todo_beneficiary.dart';
import 'package:dpatrimony/widgets/components/camera/secret_scanner.dart';
import 'package:dpatrimony/widgets/components/labels/screen_action_text.dart';
import 'package:dpatrimony/widgets/components/screen_scaffold.dart';
import 'package:dpatrimony/models/beneficiary.dart';
import 'package:dpatrimony/models/app_data.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sizer/sizer.dart';

class SecretScanningScreen extends StatefulWidget {
  static const String id_encode = 'encoding_secret_scanning_screen';
  static const String id_restore = 'restoring_secret_scanning_screen';

  final bool restoring;

  SecretScanningScreen(this.restoring);

  SecretScanningScreen.encoding() : this(false);
  SecretScanningScreen.restoring() : this(true);

  @override
  _SecretScanningScreenState createState() => _SecretScanningScreenState();
}

class _SecretScanningScreenState extends State<SecretScanningScreen> {
  @override
  Widget build(BuildContext context) {
    dynamic routeArgs(){
      if(!widget.restoring) {
        final route = ModalRoute.of(context);
        if (route == null) return SizedBox.shrink();
        return route.settings.arguments as Map<String, dynamic>;
      }
      return null;
    }

    return ScreenScaffold(
      appBarTitle: 'Secret',
      screenTitle: 'Import a secret',
      children: [
        ScreenActionText('Scan the QR code you received\nto see secret details'),
        SecretScanner(
          (v) async {
            if (widget.restoring) {
              Navigator.popAndPushNamed(context, RestorationScreen.id);
            } else {
              //list to pass data to another screen
              List<dynamic> storageData = [];
              storageData.add(routeArgs());

              Beneficiary beneficiary = QrDecoder.decodeQrString(v);
              storageData.add(beneficiary);

              Navigator.popAndPushNamed(context, SecretDetailsScreen.id_encode, arguments: storageData);
            }
          },
        ),
        Column(
          children: [
            ElevatedButton.icon(
              onPressed: () async {
                if (widget.restoring) {
                  String data =
                      'H4sIAAAAAAAAAy2MwQ7CMAxD73yF1TMHuO4f+IhuMV2hW6p0EqoQ/75M4+A4z7IMfC9AWMMQHtF6uB60ORVKoqGRgvsZi8fPuOTyrzVntTpDuHYYX8SbHTo2oubkahuW49Qogkn907Fk71sGCxMmGjFrq+fiJww3959rB6lqcTOZAAAA';
                  Beneficiary beneficiary = QrDecoder.decodeQrString(data);
                  Navigator.popAndPushNamed(context, SecretDetailsScreen.id_restore, arguments: beneficiary);
                } else {
                  //list to pass data to another screen
                  List<dynamic> storageData = [];
                  storageData.add(routeArgs());

                  String data =
                      'H4sIAAAAAAAAAy2MwQ7CMAxD73yF1TMHuO4f+IhuMV2hW6p0EqoQ/75M4+A4z7IMfC9AWMMQHtF6uB60ORVKoqGRgvsZi8fPuOTyrzVntTpDuHYYX8SbHTo2oubkahuW49Qogkn907Fk71sGCxMmGjFrq+fiJww3959rB6lqcTOZAAAA';
                  Beneficiary beneficiary = QrDecoder.decodeQrString(data);
                  storageData.add(beneficiary);

                  Navigator.popAndPushNamed(context, SecretDetailsScreen.id_encode, arguments: storageData);
                }
              },
              label: Text('REMOVE ME LATER 1'),
              icon: acceptIcon,
            ),
            ElevatedButton.icon(
              onPressed: () async {
                if (widget.restoring) {
                  String data =
                      'H4sIAAAAAAAAAy2MQQ7CMBAD77zCyrkHxLF/4BFJ14RAmkSbSihC/L1blYPXO5Zl4HsBXHGzu3sdbjpoM8qUSEUnBbczFosffk35X+vGVdsTwjKgfBFvDtTQiZaiqW9Yj9O8CJZqXw05WV8TmBmxUAkffDkXP26+mv9MO74+2G2ZAAAA';
                  Beneficiary beneficiary = QrDecoder.decodeQrString(data);
                  Navigator.popAndPushNamed(context, SecretDetailsScreen.id_restore, arguments: beneficiary);
                } else {
                  //list to pass data to another screen
                  List<dynamic> storageData = [];
                  storageData.add(routeArgs());

                  String data =
                      'H4sIAAAAAAAAAy2MQQ7CMBAD77zCyrkHxLF/4BFJ14RAmkSbSihC/L1blYPXO5Zl4HsBXHGzu3sdbjpoM8qUSEUnBbczFosffk35X+vGVdsTwjKgfBFvDtTQiZaiqW9Yj9O8CJZqXw05WV8TmBmxUAkffDkXP26+mv9MO74+2G2ZAAAA';
                  Beneficiary beneficiary = QrDecoder.decodeQrString(data);
                  storageData.add(beneficiary);

                  Navigator.popAndPushNamed(context, SecretDetailsScreen.id_encode, arguments: storageData);
                }
              },
              label: Text('REMOVE ME LATER 2'),
              icon: acceptIcon,
            ),
          ],
        ),
        Expanded(
          child: Align(
            alignment: Alignment.bottomCenter,
            child: SvgPicture.asset(
              'assets/images/qr_code_background.svg',
              height: 25.h,
            ),
          ),
        ),
        //TODO: REMOVE THIS BUTTON
      ],
    );
  }
}
