import 'dart:convert';

import 'package:dpatrimony/models/app_data.dart';
import 'package:dpatrimony/models/beneficiary.dart';
import 'package:dpatrimony/services/qr_decoder.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:dpatrimony/constants/constants.dart';
import 'package:dpatrimony/models/secret.dart';

class BeneficiaryQrCode extends StatefulWidget {
  @override
  State<BeneficiaryQrCode> createState() => _BeneficiaryQrCodeState();
}

class _BeneficiaryQrCodeState extends State<BeneficiaryQrCode> {
  FlutterSecureStorage _localStorage = new FlutterSecureStorage();
  Beneficiary beneficiary1 = QrDecoder.decodeQrString(
      'H4sIAAAAAAAAAy2MwQ7CMAxD73yF1TMHuO4f+IhuMV2hW6p0EqoQ/75M4+A4z7IMfC9AWMMQHtF6uB60ORVKoqGRgvsZi8fPuOTyrzVntTpDuHYYX8SbHTo2oubkahuW49Qogkn907Fk71sGCxMmGjFrq+fiJww3959rB6lqcTOZAAAA');
  Beneficiary beneficiary2 = QrDecoder.decodeQrString(
      'H4sIAAAAAAAAAy2MQQ7CMBAD77zCyrkHxLF/4BFJ14RAmkSbSihC/L1blYPXO5Zl4HsBXHGzu3sdbjpoM8qUSEUnBbczFosffk35X+vGVdsTwjKgfBFvDtTQiZaiqW9Yj9O8CJZqXw05WV8TmBmxUAkffDkXP26+mv9MO74+2G2ZAAAA');
  AppData testing = AppData(beneficiaries: {});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('QrCode'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(child: SizedBox()),
            Container(
                width: 300.0,
                child: Text(
                  'For testing before implementation only, will be removed later.',
                  style: styleReadOnly,
                  textAlign: TextAlign.center,
                )),
            Expanded(child: SizedBox()),
            ElevatedButton(
                onPressed: () async {
                  testing.add(beneficiary1);
                },
                child: Text('Add Beneficiary')),
            ElevatedButton(
                onPressed: () async {
                  beneficiary1.addSecret(beneficiary2.secrets.first);
                },
                child: Text('Add Beneficiary Secret')),
            ElevatedButton(
                onPressed: () async {
                  if (testing.readOnly(beneficiary2.name) == null) {
                    testing.add(beneficiary2);
                  } else {
                    Beneficiary beneficiary = testing.readOnly(beneficiary2.name);
                    beneficiary.addSecret(beneficiary2.secrets.first);
                    testing.add(beneficiary);
                  }
                  Beneficiary beneficiary = testing.readOnly(beneficiary2.name);
                  for (int i = 0; i <= beneficiary.secrets.length - 1; i++) {
                    print(beneficiary.secrets.elementAt(i).description);
                  }
                },
                child: Text('Testing Secret')),
            ElevatedButton(
                onPressed: () {
                  print(testing.readAll());
                },
                child: Text('Read All')),
            ElevatedButton(
                onPressed: () {
                  print(testing.readOnly('Mar'));
                },
                child: Text('Read Only')),
            ElevatedButton(
                onPressed: () {
                  testing.deleteAll();
                },
                child: Text('Delete All')),
            ElevatedButton(
                onPressed: () {
                  testing.deleteOnly('Mary');
                },
                child: Text('Delete Only')),
            ElevatedButton(
                onPressed: () {
                  print(testing.readKeys());
                },
                child: Text('Keys')),
            ElevatedButton(
                onPressed: () {
                  print(testing.readSpecificKey(0));
                },
                child: Text('Read Specific Key')),
            ElevatedButton(
                onPressed: () {
                  print(testing.getLength());
                },
                child: Text('Length')),
            ElevatedButton(
                onPressed: () {
                  print(QrDecoder.encodeJsonToQr(json.encode(testing.readOnly(testing.readSpecificKey(0)))));
                },
                child: Text('Generate Beneficiary Encoded')),
            Expanded(child: SizedBox()),
          ],
        ),
      ),
    );
  }
}
