import 'package:dpatrimony/constants/constants.dart';
import 'package:dpatrimony/screens/automatic_transfer/connect_wallet.dart';
import 'package:dpatrimony/screens/dictionary/download_app_screen.dart';
import 'package:dpatrimony/widgets/components/labels/decorated_icon.dart';
import 'package:dpatrimony/widgets/components/labels/decorated_label.dart';
import 'package:dpatrimony/widgets/components/labels/screen_action_text.dart';
import 'package:dpatrimony/widgets/components/screen_scaffold.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sizer/sizer.dart';

enum Option {
  setupDictionary,
  setupAutomatic,
  restoreDictionary,
  none,
}

class WelcomeScreen extends StatefulWidget {
  static const String id = 'welcome_screen';

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  Option selectedOption = Option.none;

  @override
  Widget build(BuildContext context) {
    return ScreenScaffold(
      appBarTitle: '',
      children: [
        SvgPicture.asset(
          'assets/images/welcome.svg',
          height: 30.h,
        ),
        Column(
          children: [
            Text(
              'Welcome',
              style: h1Style,
              textAlign: TextAlign.center,
            ),
            ScreenActionText('Select one of the following\noptions to begin'),
          ],
        ),
        Container(
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  DecoratedLabel.actionable(
                    label: 'Setup Dictionary',
                    onTap: () {
                      Navigator.pushNamed(context, DownloadAppScreen.id_encode); //TODO: display once only, else go straight to secrets
                      // Navigator.pushNamed(context, SecretScanningScreen.id);
                    },
                    icon: DecoratedIcon(
                      Icons.info_outline,
                      pressed: selectedOption == Option.setupDictionary,
                      onTap: () {
                        setState(() {
                          selectedOption = selectedOption == Option.setupDictionary ? Option.none : Option.setupDictionary;
                        });
                      },
                    ),
                  ),
                ],
              ),
              Space(),
              Visibility(
                child: InformationMessage(
                    'A dictionary works to encode all seed phrases of any cryptocurrency. You\'ll be able to share encoded seeds with your family, but they will only be able to decode them with the dictionary'),
                visible: selectedOption == Option.setupDictionary,
              ),
              Space(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  DecoratedLabel.actionable(
                    label: 'Setup Automatic Transfer',
                    onTap: () {
                      Navigator.pushNamed(context, ConnectWalletScreen.id);
                    },
                    icon: DecoratedIcon(
                      Icons.info_outline,
                      pressed: selectedOption == Option.setupAutomatic,
                      onTap: () {
                        setState(() {
                          selectedOption = selectedOption == Option.setupAutomatic ? Option.none : Option.setupAutomatic;
                        });
                      },
                    ),
                  ),
                ],
              ),
              Space(),
              Visibility(
                child: InformationMessage(
                    'Define automatic transfer from your cardano wallet to beneficiary wallets. This is a time-based solution and funds will be sent after prolonged inactivity'),
                visible: selectedOption == Option.setupAutomatic,
              ),
              Space(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  DecoratedLabel.actionable(
                    label: 'Restore dictionary',
                    onTap: () {
                      Navigator.pushNamed(context, DownloadAppScreen.id_restore);
                    },
                    icon: DecoratedIcon(
                      Icons.info_outline,
                      pressed: selectedOption == Option.restoreDictionary,
                      onTap: () {
                        setState(() {
                          selectedOption = selectedOption == Option.restoreDictionary ? Option.none : Option.restoreDictionary;
                        });
                      },
                    ),
                  ),
                ],
              ),
              Space(),
              Visibility(
                child: InformationMessage('Restores encoded secrets, allowing digital assets to be accessed'),
                visible: selectedOption == Option.restoreDictionary,
              ),
              // ElevatedButton(
              //   onPressed: () {
              //     Navigator.push(
              //       context,
              //       MaterialPageRoute(
              //         builder: (context) => BeneficiaryQrCode(),
              //       ),
              //     );
              //   },
              //   child: Text('QrCode'),
              // ),
            ],
          ),
        ),
      ],
    );
  }
}

class Space extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 15.0,
    );
  }
}

class InformationMessage extends StatelessWidget {
  final String text;

  InformationMessage(this.text);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Color(0xFF3492FF).withOpacity(0.08),
        borderRadius: BorderRadius.circular(8.0),
      ),
      padding: EdgeInsets.all(10.0),
      child: Text(
        text,
        textAlign: TextAlign.center,
        style: h4Style,
      ),
    );
  }
}
