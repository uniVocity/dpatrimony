import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class ButtonVerticalSpace extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(height: 0.6.h);
  }
}
