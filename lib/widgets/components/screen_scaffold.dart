import 'dart:io';

import 'package:dpatrimony/constants/constants.dart';
import 'package:dpatrimony/widgets/vertical_space.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:sizer/sizer.dart';

class ScreenScaffold extends StatefulWidget {
  final String appBarTitle;
  final String? screenTitle;
  bool? checkInternet;
  final List<Widget> children;
  bool haveInternet = true;

  ScreenScaffold({required this.appBarTitle, this.screenTitle, required this.children, this.checkInternet});

  @override
  State<ScreenScaffold> createState() => _ScreenScaffoldState();
}

class _ScreenScaffoldState extends State<ScreenScaffold> {
  @override
  Widget build(BuildContext context) {
    if (widget.checkInternet == null) widget.checkInternet = false;

    //8 > 5.sp, 14 > 11.sp, 16 > 12.5.sp, 20 > 15.5.sp, 36 > 28.sp

    Future<void> checkInternet() async {
      try {
        final result = await InternetAddress.lookup('google.com');
        if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
          widget.haveInternet = true;
        }
      } on SocketException catch (_) {
        widget.haveInternet = false;
      }
      setState(() {
        print("Tem internet: " + widget.haveInternet.toString());
      });
    }

    if (widget.checkInternet!)
      Future.delayed(const Duration(seconds: 5), () {
        checkInternet();
      });

    List<Widget> components = [];
    if (widget.screenTitle != null) {
      components.add(VerticalSpace());
      components.add(Text(
        widget.screenTitle!,
        style: h2Style,
        textAlign: TextAlign.center,
      ));
      components.add(
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: widget.children,
          ),
        ),
      );
    } else {
      components.addAll(widget.children);
    }

    List<Widget> appBarInternetChecking = [
      TextButton(
          style: ButtonStyle(),
          onPressed: () {
            if (!widget.haveInternet)
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(30.0))),
                      title: Icon(
                        Icons.wifi_off_outlined,
                        size: 80,
                      ),
                      titlePadding: EdgeInsets.only(top: 30.0),
                      content: Text(
                        "You should be\nconnected in internet",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 18.0),
                      ),
                      contentPadding: EdgeInsets.only(top: 20.0, bottom: 40.0, left: 0.0, right: 0.0),
                    );
                  });
          },
          child: AnimatedOpacity(
            opacity: !widget.haveInternet ? 1.0 : 0.0,
            duration: Duration(milliseconds: 400),
            child: Icon(Icons.wifi_off_outlined),
          )),
    ];

    AppBar? appBar;
    if (widget.appBarTitle.isNotEmpty) {
      appBar = AppBar(
        iconTheme: IconThemeData(
          color: Color(0xFF474B54),
        ),
        backgroundColor: Colors.white,
        title: Text(
          widget.appBarTitle,
          style: GoogleFonts.openSans(
            color: Color(0xFF474B54),
          ),
        ),
        actions: widget.checkInternet! ? appBarInternetChecking : [],
      );
    }

    return Scaffold(
      appBar: appBar,
      resizeToAvoidBottomInset: false,
      body: Center(
        child: Padding(
          padding: EdgeInsets.only(left: 5.w, right: 5.w, bottom: 5.w),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: components,
          ),
        ),
      ),
    );
  }
}
