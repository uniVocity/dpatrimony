import 'package:dpatrimony/constants/constants.dart';
import 'package:dpatrimony/models/beneficiary.dart';
import 'package:dpatrimony/widgets/components/inputs/input_prefix.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';

class EmailInput extends StatefulWidget {
  Beneficiary beneficiaryData;

  EmailInput({required this.beneficiaryData});

  late String? email = beneficiaryData.email;

  @override
  State<EmailInput> createState() => _EmailInputState();
}

class _EmailInputState extends State<EmailInput> {

  String? textError;
  
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onChanged: (v) {
          widget.email = v;
          setState(() {
            if(EmailValidator.validate(widget.email.toString()))
              textError = null;
            else
              textError = 'The e-mail is invalid';
          });
        },
      initialValue: widget.beneficiaryData.email,
      textAlign: TextAlign.left,
      textAlignVertical: TextAlignVertical.center,
      keyboardType: TextInputType.emailAddress,
      style: styleValue,
      decoration: kTextFieldDecoration.copyWith(
        prefixIcon: InputPrefix(Icons.email_outlined),
        hintText: 'Enter ${widget.beneficiaryData.name}\'s email.',
        errorText: textError,
      ),
    );
  }
}
