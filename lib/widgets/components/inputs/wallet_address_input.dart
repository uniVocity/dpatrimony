import 'package:dpatrimony/constants/constants.dart';
import 'package:dpatrimony/models/automatic_transfer/beneficiary.dart';
import 'package:dpatrimony/widgets/components/inputs/input_prefix.dart';
import 'package:flutter/material.dart';

class WalletAddressInput extends StatelessWidget {
  final ValueChanged<String>? onChanged;
  final Beneficiary beneficiary;

  WalletAddressInput(this.beneficiary, this.onChanged);

  @override
  Widget build(BuildContext context) {
    return TextField(
      onChanged: onChanged,
      textAlign: TextAlign.left,
      textAlignVertical: TextAlignVertical.center,
      keyboardType: TextInputType.name,
      style: styleValue,
      controller:
          TextEditingController(text: beneficiary.receivingWalletAddress),
      decoration: kTextFieldDecoration.copyWith(
        prefixIcon: InputPrefix(Icons.account_balance_wallet_outlined),
        hintText: 'Receiving wallet address.',
      ),
    );
  }
}
