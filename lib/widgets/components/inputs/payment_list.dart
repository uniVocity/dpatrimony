import 'package:dpatrimony/constants/constants.dart';
import 'package:dpatrimony/widgets/components/labels/label.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class PaymentList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Label('Payment method:'),
        SizedBox(width: 5.w),
        Expanded(
          child: InputDecorator(
            decoration: kTextFieldDecoration,
            child: DropdownButton<String>(
              underline: SizedBox.shrink(),
              isDense: true,
              isExpanded: true,
              style: styleValue,
              items: [
                DropdownMenuItem(child: Text('Cardano (1 ADA)'), value: 'ADA'),
                DropdownMenuItem(child: Text('Credit card (\$ 2.50)'), value: 'CC'),
              ],
              value: "ADA",
              onChanged: (v) {
                print(v);
              },
              // onChanged: (v) {
              //   print(v);
              // },
              autofocus: true,

              // textAlign: TextAlign.left,
              // textAlignVertical: TextAlignVertical.center,
              // keyboardType: TextInputType.emailAddress,
              // decoration: kTextFieldDecoration.copyWith(
              //   prefixIcon: InputPrefix(Icons.email),
              //   hintText: 'enter Mary\'s email.',
              // ),
            ),
          ),
        ),
      ],
    );
  }
}
