import 'package:dpatrimony/constants/constants.dart';
import 'package:dpatrimony/models/automatic_transfer/beneficiary.dart';
import 'package:dpatrimony/widgets/components/inputs/input_prefix.dart';
import 'package:flutter/material.dart';

class NameInput extends StatelessWidget {
  final ValueChanged<String>? onChanged;
  final Beneficiary beneficiary;

  NameInput(this.beneficiary, this.onChanged);

  @override
  Widget build(BuildContext context) {
    return TextField(
      onChanged: onChanged,
      textAlign: TextAlign.left,
      style: styleValue,
      controller: TextEditingController(text: beneficiary.name),
      textAlignVertical: TextAlignVertical.center,
      keyboardType: TextInputType.name,
      decoration: kTextFieldDecoration.copyWith(
        prefixIcon: InputPrefix(Icons.account_circle_outlined),
        hintText: 'Beneficiary name.',
      ),
    );
  }
}
