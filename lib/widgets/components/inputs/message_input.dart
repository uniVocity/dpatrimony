import 'package:dpatrimony/constants/constants.dart';
import 'package:dpatrimony/models/beneficiary.dart';
import 'package:dpatrimony/widgets/components/inputs/input_prefix.dart';
import 'package:flutter/material.dart';

class MessageInput extends StatelessWidget {

  Beneficiary beneficiaryData;

  MessageInput({required this.beneficiaryData});

  late String? message = beneficiaryData.message;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onChanged: (v) {
          message = v;
      },
      initialValue: beneficiaryData.message,
      textAlign: TextAlign.left,
      keyboardType: TextInputType.multiline,
      minLines: 3,
      maxLines: 3,
      style: styleValue,
      expands: false,
      decoration: kTextFieldDecoration.copyWith(
          prefixIcon: Padding(
            padding: EdgeInsets.only(bottom: 40),
            child: InputPrefix(Icons.message_outlined),
          ),
          hintText: 'Optional message for beneficiary'),
    );
  }
}