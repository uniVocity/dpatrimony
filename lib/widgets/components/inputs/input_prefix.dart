import 'package:dpatrimony/constants/constants.dart';
import 'package:flutter/material.dart';

class InputPrefix extends StatelessWidget {
  final IconData icon;

  InputPrefix(this.icon);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: Icon(
        icon,
        size: font14,
        color: Color(0xFF707583),
      ),
    );
  }
}
