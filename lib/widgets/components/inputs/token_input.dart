import 'package:dpatrimony/constants/constants.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class TokenInput extends StatefulWidget {

  String tokenValue = "ADA";

  @override
  State<TokenInput> createState() => _TokenInputState();
}

class _TokenInputState extends State<TokenInput> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 140,
      child: InputDecorator(
        decoration: kTextFieldDecoration,
        child: DropdownButton<String>(
          underline: SizedBox.shrink(),
          isDense: true,
          isExpanded: true,
          style: styleValue,
          items: [
            DropdownMenuItem(child: Text('ADA - Cardano'), value: "ADA"),
            DropdownMenuItem(child: Text('WMT - World mobile token'), value: "WMT"),
            DropdownMenuItem(child: Text('ALL - All coins'), value: "ALL"),
          ],
          value: widget.tokenValue,
          onChanged: (value) {
             setState(() {
               widget.tokenValue = value.toString();
             });
          },
        ),
      ),
    );
  }
}
