import 'dart:convert';

import 'package:dpatrimony/constants/constants.dart';
import 'package:dpatrimony/models/automatic_transfer/wallet.dart';
import 'package:dpatrimony/models/beneficiary.dart';
import 'package:dpatrimony/widgets/components/labels/label.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class WalletList extends StatefulWidget {

  Beneficiary beneficiary;
  int valueNumber;
  dynamic changeValueNumber;

  WalletList(this.beneficiary, this.valueNumber, this.changeValueNumber);

  late String valueMenu = beneficiary.secrets.elementAt(0).description;

  @override
  State<WalletList> createState() => _WalletListState();
}

class _WalletListState extends State<WalletList> {

  List<DropdownMenuItem<String>> itemsMenu(){
    List<DropdownMenuItem<String>> items = [];
    for (int i = 0; i <= widget.beneficiary.secrets.length - 1; i++) {
      items.add(DropdownMenuItem(child: Text(widget.beneficiary.secrets.elementAt(i).description), value: '$i'));
    }
    return items;
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Label('Shared secret:'),
        SizedBox(width: 5.w),
        Expanded(
          child: InputDecorator(
            decoration: kTextFieldDecoration,
            child: DropdownButton<String>(
              underline: SizedBox.shrink(),
              isDense: true,
              isExpanded: true,
              style: styleValue,
              items: itemsMenu(),
              value: '${widget.valueNumber}',
              onChanged: (v) {
                widget.changeValueNumber(int.parse(v.toString()));
              },
              autofocus: true,
            ),
          ),
        ),
      ],
    );
  }
}
