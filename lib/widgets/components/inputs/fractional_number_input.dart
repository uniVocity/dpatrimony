import 'package:dpatrimony/constants/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sizer/sizer.dart';

class FractionalNumberInput extends StatefulWidget {

  double? number;

  @override
  State<FractionalNumberInput> createState() => _FractionalNumberInputState();
}

class _FractionalNumberInputState extends State<FractionalNumberInput> {
  FocusNode _focus = new FocusNode();

  @override
  void initState() {
    super.initState();
    _focus.addListener(_onFocusChange);
  }

  void _onFocusChange() {
    if (!_focus.hasFocus) {
      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    if(widget.number == null)
      widget.number = 0;
    return Container(
      height: 6.h,
      decoration: BoxDecoration(
        border: Border.all(color: Color(0xFFC9CDD6)),
        borderRadius: BorderRadius.circular(font8),
      ),
      child: TextButton(
        onPressed: () {
          showModalBottomSheet(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.vertical(top: Radius.circular(10.0))),
            context: context,
            isScrollControlled: true,
            builder: (context) => WillPopScope(
              onWillPop: () async => false,
              child: SingleChildScrollView(
                padding: EdgeInsets.only(
                  bottom: MediaQuery.of(context).viewInsets.bottom,
                ),
                child: Container(
                  child: Container(
                    padding: EdgeInsets.all(5.0),
                    child: TextField(
                      focusNode: _focus,
                      inputFormatters: [FilteringTextInputFormatter.allow(RegExp(r'^\d+\.?\d{0,2}'))],
                      onChanged: (value) {
                        setState(() {
                          widget.number = double.parse(value);
                        });
                      },
                      autofocus: true,
                      keyboardType: TextInputType.numberWithOptions(decimal: true),
                    ),
                  ),
                ),
              ),
            ),
          );
        },
        child: Text(
          widget.number.toString(),
          style: styleValue,
        ),
      ),
    );
  }
}
