import 'package:dpatrimony/constants/constants.dart';
import 'package:flutter/material.dart';

class ShareTypeInput extends StatefulWidget {

  String shareType = PERCENTAGE;

  @override
  State<ShareTypeInput> createState() => _ShareTypeInputState();
}

const PERCENTAGE = 'Percentage';
const REMAINDER = 'Remainder';
const FIXED_AMOUNT = 'Fixed';

class _ShareTypeInputState extends State<ShareTypeInput> {

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 140,
      child: InputDecorator(
        decoration: kTextFieldDecoration,
        child: DropdownButton<dynamic>(
          underline: SizedBox.shrink(),
          isDense: true,
          isExpanded: true,
          style: styleValue,
          items: [
            DropdownMenuItem(child: Text(PERCENTAGE), value: PERCENTAGE),
            DropdownMenuItem(child: Text(FIXED_AMOUNT), value: FIXED_AMOUNT),
            DropdownMenuItem(child: Text(REMAINDER), value: REMAINDER),
          ],
          value: widget.shareType,
          onChanged: (v) {
            setState(() {
              widget.shareType = v;
            });
          },
        ),
      ),
    );
  }
}
