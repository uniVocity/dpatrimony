import 'package:dpatrimony/constants/constants.dart';
import 'package:flutter/material.dart';

class PasswordField extends StatelessWidget {
  final String hint;

  PasswordField(this.hint);
  PasswordField.password() : this('Please type a password');
  PasswordField.confirmation() : this('Confirm your password');

  @override
  Widget build(BuildContext context) {
    return TextField(
      obscureText: true,
      textAlign: TextAlign.center,
      onChanged: (value) {},
      decoration: kTextFieldDecoration.copyWith(hintText: hint),
    );
  }
}
