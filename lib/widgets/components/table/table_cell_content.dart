import 'package:dpatrimony/constants/constants.dart';
import 'package:dpatrimony/widgets/components/labels/header.dart';
import 'package:flutter/material.dart';

class TableCellContent extends StatelessWidget {
  final bool isHeader;
  final String content;
  final IconData? icon;
  final bool highlight;

  TableCellContent.header({label, icon}) : this(true, label, icon, false);
  TableCellContent.row({text, icon, highlight}) : this(false, text, icon, highlight);

  TableCellContent(this.isHeader, this.content, this.icon, this.highlight);

  @override
  Widget build(BuildContext context) {
    String content = this.content.trim();

    TextStyle style = styleValue;
    if (highlight) {
      style = styleValue.copyWith(fontWeight: FontWeight.w600);
    }

    Color color;
    if (isHeader) {
      color = Colors.white;
    } else {
      if (icon == Icons.check) {
        color = Color(0xFF0B9E43);
      } else {
        color = Color(0xFF707583);
      }
    }

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: icon == null
          ? [
              isHeader ? Header(content) : Flexible(child: Text(content, style: style, textAlign: TextAlign.center)),
            ]
          : [
              isHeader ? Header(content) : Flexible(child: Text(content, style: style, textAlign: TextAlign.center)),
              SizedBox(width: content.isEmpty ? 0 : 5),
              Icon(
                icon,
                size: font14,
                color: color,
              ),
            ],
    );
  }
}
