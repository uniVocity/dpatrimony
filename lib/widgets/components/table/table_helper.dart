import 'package:dpatrimony/constants/constants.dart';
import 'package:dpatrimony/widgets/components/table/row_type.dart';
import 'package:dpatrimony/widgets/components/table/table_cell_content.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class TableHelper {
  static List<TableRow> rows(
      {required Map<String, IconData?> headers, required List<Map<String, IconData?>> rows, Function(int)? onTap, required bool hasAddFunction}) {
    List<TableRow> out = [];

    out.add(TableRow(children: _headerRow(headers, onTap)));

    Map<String, IconData?>? footer = hasAddFunction ? null : rows.removeLast();

    int idx = 1;
    for (Map<String, IconData?> row in rows) {
      out.add(TableRow(children: _middleRow(idx++, row, onTap)));
    }

    if (footer != null) {
      out.add(TableRow(children: _footerRow(idx, footer, onTap)));
    }

    return out;
  }

  static Table addFunctionRow(String label, Function() onTap) {
    return Table(
      border: TableBorder(
        top: BorderSide(color: Color(0xFFC9CDD6), width: 1),
      ),
      children: [
        TableRow(
          children: [
            TableCell(
              child: TableRowInkWell(
                onTap: onTap,
                child: Container(
                  decoration: bottomRowDecoration,
                  child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      child: Center(
                        child: Wrap(
                          crossAxisAlignment: WrapCrossAlignment.center,
                          children: [
                            Icon(
                              Icons.add,
                              color: Color(0xFF3492FF),
                              size: font16,
                            ),
                            Text(
                              label,
                              style: styleLabel.copyWith(
                                color: Color(0xFF3492FF),
                              ),
                            ),
                          ],
                        ),
                      )),
                ),
              ),
            ),
          ],
        )
      ],
    );
  }

  static List<TableCell> _headerRow(Map<String, IconData?> headers, Function(int)? onTap) {
    return _buildRow(RowType.HEADER, 0, headers, onTap);
  }

  static List<TableCell> _middleRow(int rowIndex, Map<String, IconData?> headers, Function(int)? onTap) {
    return _buildRow(RowType.MIDDLE, rowIndex, headers, onTap);
  }

  static List<TableCell> _footerRow(int rowIndex, Map<String, IconData?> headers, Function(int)? onTap) {
    return _buildRow(RowType.FOOTER, rowIndex, headers, onTap);
  }

  static List<TableCell> _buildRow(RowType rowType, int rowIndex, Map<String, IconData?> data, Function(int)? onTap) {
    List<TableCell> out = [];

    String longest = '';
    data.forEach((value, icon) {
      if (longest.length < value.length) {
        longest = value;
      }
    });

    data.forEach((value, icon) {
      bool isLongest = longest.length == value.length;
      bool firstColumn = out.isEmpty;
      bool lastColumn = out.length + 1 == data.length;

      double? height;
      bool isHeader = false;
      List<BoxDecoration> decorations;
      switch (rowType) {
        case RowType.HEADER:
          height = 5.h;
          isHeader = true;
          onTap = null;
          decorations = [columnTopLeftDecoration, columnTopMiddleDecoration, columnTopRightDecoration];
          break;
        case RowType.MIDDLE:
          decorations = [columnBottomMiddleDecoration, columnBottomMiddleDecoration, columnBottomMiddleDecoration];
          break;
        case RowType.FOOTER:
          decorations = [columnBottomLeftDecoration, columnBottomMiddleDecoration, columnBottomRightDecoration];
          break;
      }

      Border leftBorder = Border(
        left: BorderSide(color: Color(0xFFC9CDD6), width: 1),
        right: BorderSide(color: Color(0xFFC9CDD6), width: 1),
      );

      out.add(
        TableCell(
          verticalAlignment: (isLongest || isHeader) ? TableCellVerticalAlignment.middle : TableCellVerticalAlignment.fill,
          child: TableRowInkWell(
            onTap: () {
              if (onTap != null) {
                onTap!.call(rowIndex);
              }
            },
            child: Container(
              height: height,
              decoration: firstColumn
                  ? decorations[0]
                  : !lastColumn
                      ? decorations[1].copyWith(border: leftBorder)
                      : decorations[2],
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: isHeader
                    ? TableCellContent.header(label: value, icon: icon)
                    : TableCellContent.row(
                        text: value,
                        icon: icon,
                        highlight: firstColumn,
                      ),
              ),
            ),
          ),
        ),
      );
    });

    return out;
  }

  static List<Widget> getTableHeaderAndBody(List<TableRow> rows, Table? functionRow, String? emptyMessage) {
    List<Table> out = [];
    out.add(Table(
      children: [
        rows[0],
      ],
    ));

    if (rows.length == 1 && emptyMessage != null) {
      rows.add(TableRow(children: _middleRow(1, {emptyMessage: null}, (v) {})));
    }

    if (rows.length > 1) {
      out.add(Table(
        children: rows.sublist(1, rows.length),
      ));
    }

    if (functionRow != null) {
      out.add(functionRow);
    }

    List<Table> rowsAndFooter = out.sublist(1, out.length);

    return [
      out[0], //header
      Expanded(
        child: Scrollbar(
          child: SingleChildScrollView(
            child: Column(
              children: rowsAndFooter,
            ),
          ),
        ),
      ),
    ];
  }
}
