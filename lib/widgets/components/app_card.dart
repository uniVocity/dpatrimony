import 'package:dpatrimony/constants/constants.dart';
import 'package:flutter/material.dart';

class AppCard extends StatelessWidget {
  final Widget child;
  final double? height;
  final EdgeInsetsGeometry? padding;

  AppCard({required this.child, this.height, this.padding});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      padding: padding,
      decoration: BoxDecoration(
        color: Color(0xFF1F232C).withOpacity(0.08),
        borderRadius: BorderRadius.all(Radius.circular(font8)),
        border: Border.all(color: Color(0XFF1F232C).withOpacity(0.3)),
      ),
      child: child,
    );
  }
}
