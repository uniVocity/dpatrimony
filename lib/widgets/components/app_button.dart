import 'package:dpatrimony/constants/constants.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class AppButton extends StatelessWidget {
  double radius = font8;

  final VoidCallback onPressed;
  final String label;
  final IconData? icon;
  final bool outlined;
  final Color? color;

  AppButton(this.outlined, this.color, this.icon, this.label, this.onPressed);

  AppButton.outlined({icon, required label, required onPressed}) : this(true, null, icon, label, onPressed);

  AppButton.full({icon, required label, required onPressed}) : this(false, null, icon, label, onPressed);

  AppButton.colored({icon, required label, required color, required onPressed}) : this(true, color, icon, label, onPressed);

  AppButton.red({icon, required label, required onPressed}) : this.colored(icon: icon, label: label, color: Color(0xFFE94256), onPressed: onPressed);

  AppButton.black({icon, required label, required onPressed}) : this.colored(icon: icon, label: label, color: Color(0xFF1F232C), onPressed: onPressed);

  Row buttonContent() {
    Color color = !outlined
        ? Colors.white
        : this.color != null
            ? this.color!
            : Color(0xFF3492FF);

    List<Widget> elements = [];

    if (icon != null) {
      elements.add(Icon(
        icon,
        size: font18,
        color: color,
      ));
      elements.add(SizedBox(width: 8));
    }

    elements.add(Container(
      child: Text(
        label,
        style: TextStyle(
          fontSize: font16,
          fontStyle: FontStyle.normal,
          fontWeight: FontWeight.w400,
          color: color,
        ),
      ),
    ));

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: elements,
    );
  }

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      style: ElevatedButton.styleFrom(padding: EdgeInsets.zero, shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(radius))),
      child: Ink(
        decoration: gradientDecoration(color != null ? Colors.white : Color(0xFF33A9FF), color != null ? Colors.white : Color(0xFF3377FF), radius),
        child: Container(
          height: 6.h,
          width: 70.w,
          child: !outlined
              ? buttonContent()
              : Container(
                  margin: EdgeInsets.all(1),
                  child: Ink(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(radius),
                    ),
                    child: buttonContent(),
                  ),
                ),
        ),
      ),
    );
  }
}
