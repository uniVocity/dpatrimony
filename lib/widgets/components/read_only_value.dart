import 'package:dpatrimony/constants/constants.dart';
import 'package:flutter/material.dart';

class ReadOnlyValue extends StatelessWidget {
  final String value;

  ReadOnlyValue(this.value);

  @override
  Widget build(BuildContext context) {
    return Text(
      value,
      style: styleReadOnly,
    );
  }
}
