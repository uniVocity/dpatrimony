import 'package:dpatrimony/constants/constants.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:sizer/sizer.dart';

import 'card_decorated_icon.dart';

class CardDecoratedLabel extends StatelessWidget {
  final String label;
  final CardDecoratedIcon? icon;
  final VoidCallback onTap;
  final String? details;
  final EdgeInsets? iconPadding;

  CardDecoratedLabel.actionable({required this.label, this.details, required this.onTap, this.icon, this.iconPadding});

  CardDecoratedLabel(label, icon) : this.actionable(label: label, onTap: () {}, icon: icon);

  @override
  Widget build(BuildContext context) {
    List<Widget> titleContent = [
      Text(
        label,
        style: GoogleFonts.openSans(
          fontSize: font18,
          fontWeight: FontWeight.w700,
          color: Color(0xFF1F232C),
        ),
      )
    ];
    if (details != null) {
      String text = details!;
      if (text.length > 25) {
        String first = text.substring(0, 13);
        String last = text.substring(text.length - 8, text.length);
        text = first + '...' + last;
      }

      titleContent.add(
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 50),
          child: Text(
            text,
            style: GoogleFonts.openSans(
              fontSize: font10,
              fontWeight: FontWeight.w400,
              color: Color(0xFF1F232C),
            ),
          ),
        ),
      );
    }

    return Container(
      height: 7.h,
      child: Stack(
        children: [
          Center(
            child: InkWell(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: titleContent,
              ),
              onTap: onTap,
            ),
          ),
          icon == null
              ? SizedBox()
              : Positioned(
                  right: 1,
                  child: Padding(
                    padding: iconPadding != null ? iconPadding! : EdgeInsets.symmetric(vertical: 1.1.h),
                    child: icon,
                  ),
                )
        ],
      ),
    );
  }
}
