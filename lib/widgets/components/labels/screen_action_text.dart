import 'package:dpatrimony/constants/constants.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class ScreenActionText extends StatelessWidget {
  final String text;

  ScreenActionText(this.text);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 5.w, bottom: 5.w),
      child: Text(
        text,
        style: h3Style,
        textAlign: TextAlign.center,
      ),
    );
  }
}
