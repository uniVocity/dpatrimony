import 'package:dpatrimony/constants/constants.dart';
import 'package:dpatrimony/widgets/components/labels/decorated_icon.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:sizer/sizer.dart';

class DecoratedLabel extends StatelessWidget {
  final String label;
  final DecoratedIcon icon;
  final VoidCallback onTap;

  DecoratedLabel.actionable({required this.label, required this.onTap, required this.icon});

  DecoratedLabel(label, icon) : this.actionable(label: label, onTap: () {}, icon: icon);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 7.h,
      width: 80.w,
      decoration: new BoxDecoration(
        color: Color(0xFF353E4C),
        borderRadius: new BorderRadius.all(Radius.circular(80.w)),
      ),
      child: Stack(
        children: [
          Center(
            child: InkWell(
              child: Text(
                label,
                style: GoogleFonts.openSans(
                  fontSize: font16,
                  color: Colors.white,
                ),
              ),
              onTap: onTap,
            ),
          ),
          Container(
            alignment: Alignment.centerRight,
            margin: EdgeInsets.only(right: 10),
            child: icon,
          )
        ],
      ),
    );
  }
}
