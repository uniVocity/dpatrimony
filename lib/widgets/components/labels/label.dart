import 'package:dpatrimony/constants/constants.dart';
import 'package:flutter/material.dart';

class Label extends StatelessWidget {
  final String label;

  Label(this.label);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Text(
        label,
        style: styleLabel,
      ),
    );
  }
}
