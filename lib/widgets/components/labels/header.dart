import 'package:dpatrimony/constants/constants.dart';
import 'package:flutter/material.dart';

class Header extends StatelessWidget {
  final String label;

  Header(this.label);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Text(
        label,
        style: styleLabel.copyWith(color: Colors.white),
      ),
    );
  }
}
