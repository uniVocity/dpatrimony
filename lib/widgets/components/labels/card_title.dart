import 'package:dpatrimony/constants/constants.dart';
import 'package:flutter/material.dart';

class CardTitle extends StatelessWidget {
  final String title;

  CardTitle(this.title);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
      child: Text(
        title,
        style: styleCardTitle,
      ),
    );
  }
}
