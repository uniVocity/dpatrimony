import 'package:dpatrimony/constants/constants.dart';
import 'package:flutter/material.dart';

class CardDecoratedIcon extends StatelessWidget {
  final IconData iconData;
  final bool pressed;
  final VoidCallback? onTap;

  CardDecoratedIcon(this.iconData, {required this.pressed, required this.onTap});
  CardDecoratedIcon.raised(iconData, onTap) : this(iconData, pressed: false, onTap: onTap);
  CardDecoratedIcon.pressed(iconData, onTap) : this(iconData, pressed: true, onTap: onTap);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        child: Icon(
          iconData,
          size: font20,
          color: pressed ? Colors.white : Color(0xFF707583),
        ),
        width: font34 + 5,
        height: font34 + 5,
        alignment: Alignment.center,
        decoration:
            pressed ? gradientDecoration(Color(0xFF33A9FF), Color(0xFF3377FF), font36) : gradientDecoration(Color(0xFFF2F2F2), Color(0xFFF2F2F2), font36),
      ),
    );
  }
}
