import 'package:dpatrimony/constants/constants.dart';
import 'package:flutter/material.dart';

class DecoratedIcon extends StatelessWidget {
  final IconData iconData;
  final bool pressed;
  final VoidCallback? onTap;

  DecoratedIcon(this.iconData, {required this.pressed, required this.onTap});
  DecoratedIcon.raised(iconData, onTap) : this(iconData, pressed: false, onTap: onTap);
  DecoratedIcon.pressed(iconData, onTap) : this(iconData, pressed: true, onTap: onTap);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        width: font34,
        height: font34,
        alignment: Alignment.center,
        decoration: new BoxDecoration(
          color: pressed ? Colors.white.withOpacity(0.15) : Color(0xFF353E4C),
          borderRadius: new BorderRadius.all(Radius.circular(font34)),
        ),
        child: Icon(
          iconData,
          size: font20,
          color: Colors.white,
        ),
      ),
    );
  }
}
