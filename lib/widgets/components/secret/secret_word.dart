import 'package:dpatrimony/constants/constants.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class SecretWord extends StatelessWidget {
  final int position;
  final String word;

  SecretWord(this.position, this.word);

  @override
  Widget build(BuildContext context) {
    String pos = position.toString();
    return new Container(
      margin: EdgeInsets.all(0.8.w),
      padding: EdgeInsets.only(left: 1.w, right: 2.w, top: 0.5.h, bottom: 0.5.h),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.blueAccent, width: 1),
        color: Color(0xFF3492FF),
        borderRadius: BorderRadius.all(
          Radius.circular(1.w),
        ),
      ),
      child: Wrap(
        children: [
          Text(
            '$pos',
            style: TextStyle(color: Colors.white.withOpacity(0.75), fontSize: font8, fontWeight: FontWeight.w400),
          ),
          SizedBox(width: 5),
          Text(
            word,
            style: TextStyle(color: Colors.white, fontSize: font12, fontWeight: FontWeight.w500),
          )
        ],
      ),
    );
  }
}
