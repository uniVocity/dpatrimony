import 'package:dpatrimony/constants/constants.dart';
import 'package:dpatrimony/widgets/components/secret/secret_word.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import '../app_card.dart';

class SecretWordList extends StatelessWidget {
  final List<String> wordList;
  final String description;

  SecretWordList({required this.description, required this.wordList});

  @override
  Widget build(BuildContext context) {
    List<SecretWord> list = [];
    for (int i = 0; i < wordList.length; i++) {
      String word = wordList[i];
      list.add(SecretWord(i + 1, word));
    }

    return AppCard(
      padding: EdgeInsets.all(1.5.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            height: 5.h,
            padding: EdgeInsets.all(1.h),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(font8)),
            ),
            child: Text(
              'Encoded $description',
              style: h4Style,
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(height: 1.h),
          Wrap(
            alignment: WrapAlignment.center,
            children: list,
          ),
        ],
      ),
    );
  }
}
