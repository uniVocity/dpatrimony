import 'package:dpatrimony/widgets/vertical_space.dart';
import 'package:flutter/material.dart';
import 'package:dpatrimony/constants/constants.dart';

class WalletDetailsCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 300.0,
      padding: EdgeInsets.all(20.0),
      child: Column(
        children: [
          Container(
            height: 180.0,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(child: SizedBox()),
                ElevatedButton.icon(
                  onPressed: () {},
                  icon: walletIcon,
                  label: Text(
                    'wallet1',
                    style: styleTextReadWhite,
                  ),
                ),
                Expanded(child: SizedBox()),
                Text(
                  'Address: addr1345fgvw32a3saasx',
                  style: styleTextRead,
                ),
                Expanded(child: SizedBox()),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('Balances:', style: styleTextRead),
                    Text('1000 ADA', style: styleTextRead),
                    Text('200 WMT', style: styleTextRead),
                    Text('44 USD', style: styleTextRead),
                  ],
                ),
                Expanded(child: SizedBox()),
              ],
            ),
          ),
          VerticalSpace(),
          ElevatedButton.icon(
            onPressed: () {
              Navigator.pop(context);
            },
            label: Text('Return'),
            icon: previousIcon,
          ),
        ],
      ),
    );
  }
}
