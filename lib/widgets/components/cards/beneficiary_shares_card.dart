import 'package:dpatrimony/constants/constants.dart';
import 'package:dpatrimony/models/automatic_transfer/beneficiary.dart';
import 'package:dpatrimony/models/automatic_transfer/share.dart';
import 'package:dpatrimony/models/automatic_transfer/token.dart';
import 'package:dpatrimony/widgets/components/app_button.dart';
import 'package:dpatrimony/widgets/components/inputs/fractional_number_input.dart';
import 'package:dpatrimony/widgets/components/inputs/share_type_input.dart';
import 'package:dpatrimony/widgets/components/inputs/token_input.dart';
import 'package:dpatrimony/widgets/components/labels/card_decorated_label.dart';
import 'package:dpatrimony/widgets/vertical_space.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'details_card.dart';

class BeneficiarySharesCard extends StatefulWidget {
  dynamic addShare;
  dynamic updateState;
  Beneficiary beneficiary;
  int? shareIndex;
  dynamic changeValueModificationToFalse;

  BeneficiarySharesCard(this.addShare, this.updateState, this.beneficiary, [this.shareIndex, this.changeValueModificationToFalse]);

  @override
  State<BeneficiarySharesCard> createState() => _BeneficiarySharesCardState();
}

class _BeneficiarySharesCardState extends State<BeneficiarySharesCard> {
  TokenInput tokenInput = TokenInput();
  ShareTypeInput shareTypeInput = ShareTypeInput();
  FractionalNumberInput fractionalNumberInput = FractionalNumberInput();

  @override
  Widget build(BuildContext context) {
    List<Share> shareValues = [];
    List<Token> tokenValues = [];

    widget.beneficiary.shares.forEach((key, value) {
      shareValues.add(value);
      tokenValues.add(key);
    });

    if (widget.shareIndex != null) {
      if (fractionalNumberInput.number == null) {
          switch (shareValues[(int.parse('${widget.shareIndex}'))].shareType) {
            case ShareType.Percentage:
              shareTypeInput.shareType = 'Percentage';
              break;
            case ShareType.FixedAmount:
              shareTypeInput.shareType = 'Fixed';
              break;
            case ShareType.Remainder:
              shareTypeInput.shareType = 'Remainder';
              break;
          }

          fractionalNumberInput.number = shareValues[(int.parse('${widget.shareIndex}'))].amount;
          tokenInput.tokenValue = tokenValues[(int.parse('${widget.shareIndex}'))].ticker;
      }
    }

    void removeShare() {
      widget.beneficiary.removeShare(tokenValues[(int.parse('${widget.shareIndex}'))]);
      widget.changeValueModificationToFalse();
      widget.updateState();
    }

    void modifyShare() {
      String tokenDescription = '';
      switch (tokenInput.tokenValue) {
        case 'ADA':
          tokenDescription = 'Cardano';
          break;
        case 'WMT':
          tokenDescription = 'World mobile Token';
          break;
        case 'ALL':
          tokenDescription = 'All coins';
      }
      Token token = Token(description: tokenDescription, hash: tokenInput.tokenValue.hashCode.toString(), ticker: tokenInput.tokenValue);

      switch (shareTypeInput.shareType) {
        case 'Percentage':
          shareValues[(int.parse('${widget.shareIndex}'))].shareType = ShareType.Percentage;
          break;
        case 'Fixed':
          shareValues[(int.parse('${widget.shareIndex}'))].shareType = ShareType.FixedAmount;
          break;
        case 'Remainder':
          shareValues[(int.parse('${widget.shareIndex}'))].shareType = ShareType.Remainder;
          break;
      }
      shareValues[(int.parse('${widget.shareIndex}'))].amount = fractionalNumberInput.number!.toDouble();

      widget.beneficiary.removeShare(tokenValues[(int.parse('${widget.shareIndex}'))]);
      widget.beneficiary.addShare(token, shareValues[(int.parse('${widget.shareIndex}'))]);
      widget.updateState();
      widget.changeValueModificationToFalse();
    }

    dynamic errorOnSavingShare(){
      return showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(20.0))),
          content: Container(
            height: 150.0,
            width: 50.0,
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.only(bottom: 10.0),
                    child: Text(
                      'Error on Saving',
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.red, fontSize: 25, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Text('Amount is invalid'),
                  Padding(
                    padding: EdgeInsets.only(top: 10.0),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(primary: Colors.black54),
                      onPressed: () => Navigator.pop(context, 'OK'),
                      child: Text('Ok'),
                    ),
                  ),
                ],
              ),
            ),
          ),
          contentPadding: EdgeInsets.only(top: 5.0, bottom: 5.0),
        ),
      );
    }

    List<Widget> appButtons = widget.shareIndex == null
        ? [
            AppButton.full(
              label: 'Save',
              onPressed: () {
                if(fractionalNumberInput.number !> 0 || shareTypeInput.shareType == 'Remainder'){
                  widget.addShare(tokenInput.tokenValue, shareTypeInput.shareType, fractionalNumberInput.number);
                  Navigator.pop(context);
                }else
                  errorOnSavingShare();

              },
            ),
          ]
        : [
            AppButton.full(
              label: 'Save',
              onPressed: () {
                if(fractionalNumberInput.number !> 0 || shareTypeInput.shareType == 'Remainder'){
                  modifyShare();
                  Navigator.pop(context);
                }else
                  errorOnSavingShare();
              },
            ),
            SizedBox(height: 1.1.h),
            AppButton.red(
              label: 'Remove',
              onPressed: () {
                removeShare();
                Navigator.pop(context);
              },
            ),
          ];


    return DetailsCard(children: [
      CardDecoratedLabel(
        'Add share',
        null,
      ),
      VerticalSpace(),
      Container(
        child: Row(
          children: [
            LabeledInput('Token', tokenInput),
          ],
        ),
      ),
      VerticalSpace(),
      Container(
        child: Row(
          children: [
            LabeledInput('Share type', shareTypeInput),
            SizedBox(width: 20),
            LabeledInput('Amount', fractionalNumberInput),
          ],
        ),
      ),
      VerticalSpace(),
      Column(
        children: appButtons,
      ),
    ]);
  }
}

class LabeledInput extends StatelessWidget {
  final String label;
  final Widget input;

  LabeledInput(this.label, this.input);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Padding(
            padding: EdgeInsets.only(left: 1.5.w),
            child: Text(
              label,
              style: styleInputLabel,
            ),
          ),
          input,
        ],
      ),
    );
  }
}
