import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class DetailsCard extends StatelessWidget {
  final List<Widget> children;

  DetailsCard({required this.children});

  @override
  Widget build(BuildContext context) {
    List<Widget> components = [];
    components.add(
      Container(
        width: 30,
        child: SizedBox(height: 3),
        decoration: BoxDecoration(
          color: Color(0xFFC9CDD6),
          borderRadius: BorderRadius.circular(2),
        ),
      ),
    );

    components.addAll(children);

    return Container(
      padding: EdgeInsets.only(left: 5.w, right: 5.w, bottom: 5.w, top: 5),
      child: Column(
        children: components,
      ),
    );
  }
}
