import 'dart:io';

import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

import 'camera_flash_button.dart';
import 'camera_side_button.dart';

import 'package:sizer/sizer.dart';

class SecretScanner extends StatefulWidget {
  final ValueChanged<String> onChanged;

  SecretScanner(this.onChanged);

  @override
  _SecretScannerState createState() => _SecretScannerState();
}

class _SecretScannerState extends State<SecretScanner> {
  String? data;
  QRViewController? controller;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');

  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller?.pauseCamera();
    } else {
      controller?.resumeCamera();
    }
  }

  @override
  Widget build(BuildContext context) {
    Widget qrView = _buildQrView(context);

    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Center(
            child: Container(
              width: 35.h,
              height: 35.h,
              child: Container(
                decoration: BoxDecoration(
                  shape: BoxShape.rectangle,
                  border: Border.all(
                    color: Color(0xFFE0E0E0),
                    width: 2,
                  ),
                ),
                child: qrView,
              ),
              decoration: ShapeDecoration(
                  shape: QrScannerOverlayShape(
                      borderColor: Color(0xFF3492FF),
                      overlayColor: ThemeData.light().scaffoldBackgroundColor,
                      borderRadius: 0,
                      borderLength: 20,
                      borderWidth: 2.5,
                      cutOutSize: 35.h - 10)),
            ),
          ),
          Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CameraFlashButton(controller),
                SizedBox(width: 25),
                CameraSideButton(controller),
              ],
            ),
          ),
          //       CameraButton(
          //         onPressed: () async {
          //           await controller?.pauseCamera();
          //         },
          //         icon: Icon(Icons.pause, color: colorMain),
          //       ),
          //       CameraButton(
          //         onPressed: () async {
          //           await controller?.resumeCamera();
          //         },
          //         icon: Icon(Icons.play_arrow, color: colorMain),
          //       )
          // )
        ],
      ),
    );
  }

  Widget _buildQrView(BuildContext context) {
    return QRView(
      key: qrKey,
      onQRViewCreated: _onQRViewCreated,
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    setState(() {
      this.controller = controller;
    });
    controller.scannedDataStream.listen((scanData) {
      setState(() {
        processScanData(scanData);
      });
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  void processScanData(Barcode? scanData) {
    if (scanData?.format == BarcodeFormat.qrcode) {
      data = scanData?.code;
      if (data != null) {
        widget.onChanged(data!);
      }
    } else {
      data = null;
    }
    data = null;
  }
}
