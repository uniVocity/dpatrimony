import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

import 'camera_button.dart';

class CameraFlashButton extends StatefulWidget {
  final QRViewController? controller;

  CameraFlashButton(this.controller);

  @override
  _CameraFlashButtonState createState() => _CameraFlashButtonState();
}

class _CameraFlashButtonState extends State<CameraFlashButton> {
  static const IconData ON = Icons.flash_on_outlined;
  static const IconData OFF = Icons.flash_off_outlined;

  IconData icon = OFF;

  @override
  Widget build(BuildContext context) {
    return CameraButton(
        onPressed: () async {
          await widget.controller?.toggleFlash();
          setState(() {});
        },
        icon: FutureBuilder(
          future: widget.controller?.getFlashStatus(),
          builder: (context, snapshot) {
            icon = snapshot.data == true ? ON : OFF;
            return Icon(icon, color: Color(0xFF707583));
          },
        ));
  }
}
