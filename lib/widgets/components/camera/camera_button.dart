import 'package:dpatrimony/constants/constants.dart';
import 'package:flutter/material.dart';

class CameraButton extends StatelessWidget {
  final Widget icon;
  final VoidCallback onPressed;

  CameraButton({required this.icon, required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Container(
        width: font34,
        height: font34,
        alignment: Alignment.center,
        decoration: new BoxDecoration(
          color: Color(0xFFF2F2F2),
          borderRadius: new BorderRadius.all(Radius.circular(font34)),
        ),
        child: IconButton(
          onPressed: onPressed,
          iconSize: font14,
          padding: EdgeInsets.zero,
          constraints: BoxConstraints(),
          icon: icon,
          splashRadius: 20,
        ));
  }
}
