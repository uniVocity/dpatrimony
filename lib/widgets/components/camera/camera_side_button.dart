import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

import 'camera_button.dart';

class CameraSideButton extends StatefulWidget {
  final QRViewController? controller;

  CameraSideButton(this.controller);

  @override
  _CameraSideButtonState createState() => _CameraSideButtonState();
}

class _CameraSideButtonState extends State<CameraSideButton> {
  static const IconData FRONT = Icons.camera_front;
  static const IconData BACK = Icons.camera_rear;

  IconData icon = FRONT;

  @override
  Widget build(BuildContext context) {
    return CameraButton(
        onPressed: () async {
          await widget.controller?.flipCamera();
          setState(() {});
        },
        icon: FutureBuilder(
          future: widget.controller?.getCameraInfo(),
          builder: (context, snapshot) {
            CameraFacing? facing = snapshot.data as CameraFacing?;
            if (facing != null) {
              return Icon(facing == CameraFacing.back || facing == CameraFacing.unknown ? BACK : FRONT, color: Color(0xFF707583));
            } else {
              return Icon(Icons.sync, color: Color(0xFF707583));
            }
          },
        ));
  }
}
