import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class VerticalSpace extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(height: 2.6.h);
  }
}
