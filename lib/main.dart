import 'package:dpatrimony/constants/constants.dart';
import 'package:dpatrimony/models/automatic_transfer/automatic_transfer.dart';
import 'package:dpatrimony/models/automatic_transfer/automatic_transfers.dart';
import 'package:dpatrimony/models/automatic_transfer/beneficiary.dart';
import 'package:dpatrimony/models/automatic_transfer/share.dart';
import 'package:dpatrimony/models/automatic_transfer/time_to_wait.dart';
import 'package:dpatrimony/screens/automatic_transfer/beneficiary_management.dart';
import 'package:dpatrimony/screens/automatic_transfer/beneficiary_shares_screen.dart';
import 'package:dpatrimony/screens/automatic_transfer/connect_wallet.dart';
import 'package:dpatrimony/screens/automatic_transfer/connected_wallet_screen.dart';
import 'package:dpatrimony/screens/dictionary/download_app_screen.dart';
import 'package:dpatrimony/screens/dictionary/restoration_screen.dart';
import 'package:dpatrimony/screens/dictionary/secret_details_screen.dart';
import 'package:dpatrimony/screens/dictionary/secret_scanning_screen.dart';
import 'package:dpatrimony/screens/dictionary/shared_secret_screen.dart';
import 'package:dpatrimony/screens/welcome_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';

import 'screens/dictionary/activation_screen.dart';

void configLoading() {
  EasyLoading.instance
    ..displayDuration = const Duration(milliseconds: 2000)
    ..indicatorType = EasyLoadingIndicatorType.fadingCircle
    ..loadingStyle = EasyLoadingStyle.custom
    ..textStyle = TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w500)
    ..indicatorSize = 45.0
    ..backgroundColor = Color(0xFF353E4C).withOpacity(0.9)
    ..indicatorColor = Colors.white
    ..textColor = Colors.white
    ..radius = 10.0
    ..maskColor = Colors.blue.withOpacity(0.2)
    ..userInteractions = false;
}

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
    [
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ],
  ).then((v) {
    // print('font8  = $font8 ');
    // print('font12 = $font12');
    // print('font14 = $font14');
    // print('font16 = $font16');
    // print('font18 = $font18');
    // print('font20 = $font20');
    // print('font36 = $font36');
  });

  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => Beneficiary()),
        ChangeNotifierProvider(create: (context) => Share()),
        ChangeNotifierProvider(create: (context) => TimeToWait()),
      ],
      child: MyApp(),
    ),
  );

  configLoading();
}

class MyApp extends StatelessWidget {
  static void goBackHome(context) {
    Navigator.popUntil(context, ModalRoute.withName(WelcomeScreen.id));
  }

  @override
  Widget build(BuildContext context) {
    return Sizer(builder: (context, orientation, deviceType) {
      return MaterialApp(
        title: 'dPatrimony',
        theme: ThemeData.light().copyWith(
          textTheme: GoogleFonts.openSansTextTheme(
            Theme.of(context).textTheme,
          ),
          textButtonTheme: TextButtonThemeData(
            style: TextButton.styleFrom(
              primary: Color(0xFF222222),
            ),
          ),
          elevatedButtonTheme: ElevatedButtonThemeData(
            style: ElevatedButton.styleFrom(
              primary: Color(0xFF18A0FB),
            ),
          ),
        ),
        routes: {
          WelcomeScreen.id: (context) => WelcomeScreen(),
          SecretScanningScreen.id_encode: (context) => SecretScanningScreen.encoding(),
          SecretScanningScreen.id_restore: (context) => SecretScanningScreen.restoring(),
          SharedSecretScreen.id: (context) => SharedSecretScreen({}),
          ActivationScreen.id: (context) => ActivationScreen(),
          ConnectedWalletsScreen.id: (context) => ConnectedWalletsScreen(),
          BeneficiaryManagementScreen.id: (context) => BeneficiaryManagementScreen(),
          ConnectWalletScreen.id: (context) => ConnectWalletScreen(),
          DownloadAppScreen.id_encode: (context) => DownloadAppScreen.encoding(),
          DownloadAppScreen.id_restore: (context) => DownloadAppScreen.restoring(),
          RestorationScreen.id: (context) => RestorationScreen(),
          SecretDetailsScreen.id_encode: (context) => SecretDetailsScreen.encoding(),
          SecretDetailsScreen.id_restore: (context) => SecretDetailsScreen.restoring(),
        },
        initialRoute: WelcomeScreen.id,
        builder: EasyLoading.init(),
      );
    });
  }
}
