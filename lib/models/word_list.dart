enum WordList {
  English,
  Japanese,
  Korean,
  Spanish,
  Chinese_Simplified,
  Chinese_Traditional,
  French,
  Italian,
  Czech,
  Portuguese,
}
