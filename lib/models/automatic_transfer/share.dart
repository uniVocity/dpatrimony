import 'package:flutter/material.dart';

enum ShareType { Percentage, FixedAmount, Remainder }

class Share extends ChangeNotifier {
  ShareType? _shareType;
  double? _amount;

  ShareType get shareType => _shareType ?? ShareType.Percentage;

  set shareType(ShareType shareType) {
    _shareType = shareType;
    stateUpdated();
  }

  double get amount => _amount ?? 0;

  set amount(double amount) {
    _amount = amount;
    stateUpdated();
  }

  void stateUpdated() {
    if (_shareType == ShareType.Percentage) {
      if (amount > 100) {
        amount = 100.0;
      }
    } else if (_shareType == ShareType.Remainder) {
      if (amount > 0) {
        amount = 0.0;
      }
      print(amount);
    }
    notifyListeners();
  }
}
