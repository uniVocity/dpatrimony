import 'package:dpatrimony/models/automatic_transfer/share.dart';
import 'package:dpatrimony/models/automatic_transfer/token.dart';
import 'package:flutter/material.dart';

class Beneficiary extends ChangeNotifier {
  String? _name;
  String? _receivingWalletAddress;
  Map<Token, Share> _shares = {};

  String get name => _name ?? '';

  set name(String name) {
    _name = name;
    notifyListeners();
  }

  String get receivingWalletAddress => _receivingWalletAddress ?? '';

  set receivingWalletAddress(String receivingWalletAddress) {
    _receivingWalletAddress = receivingWalletAddress;
    notifyListeners();
  }

  Map<Token, Share> get shares => Map.unmodifiable(_shares);

  void addShare(Token token, Share share) {
    _shares[token] = share;
    notifyListeners();
  }

  void removeShare(Token token) {
    _shares.remove(token);
    notifyListeners();
  }

  get shareDescription {
    String out = '';
    shares.forEach((token, share) {
      String description;
      switch (share.shareType) {
        case ShareType.Percentage:
          description = share.amount.toString() + '%';
          break;
        case ShareType.FixedAmount:
          description = share.amount.toString();
          break;
        case ShareType.Remainder:
          description = 'Remainder';
          break;
      }
      description += ' ${token.ticker}';

      if (out.length > 0) {
        out += ', ';
      }

      out += description;
    });
    return out;
  }
}
