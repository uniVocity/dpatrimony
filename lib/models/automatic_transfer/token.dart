class Token {
  String description;
  String hash;
  String ticker;

  Token({required this.description, required this.hash, required this.ticker});
}
