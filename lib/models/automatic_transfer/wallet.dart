import 'package:dpatrimony/models/automatic_transfer/token.dart';

class Wallet {
  String name;
  String address;
  bool _displayingBalances = false;

  Map<Token, String> balance = {}; //symbol + amount

  Wallet({required this.name, required this.address});

  void toggleDisplayingBalances() {
    _displayingBalances = !_displayingBalances;
  }

  bool get displayingBalances => _displayingBalances;
}
