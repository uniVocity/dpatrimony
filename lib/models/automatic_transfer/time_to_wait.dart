import 'package:dpatrimony/models/automatic_transfer/period.dart';
import 'package:flutter/material.dart';

class TimeToWait extends ChangeNotifier {
  int? _amount;

  Period? _period;

  int get amount => _amount ?? 1;

  set amount(int amount) {
    _amount = amount;
    notifyListeners();
  }

  Period get period => _period ?? Period.years;

  set period(Period period) {
    _period = period;
    notifyListeners();
  }
}
