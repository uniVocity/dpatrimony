import 'package:dpatrimony/models/automatic_transfer/automatic_transfer.dart';
import 'package:dpatrimony/models/automatic_transfer/wallet.dart';
import 'package:flutter/material.dart';

class AutomaticTransfers extends ChangeNotifier {
  static AutomaticTransfers _instance = AutomaticTransfers._();

  final Map<String, AutomaticTransfer> _automaticTransfers = {};

  AutomaticTransfers._();

  static AutomaticTransfers get instance => _instance;

  AutomaticTransfer addAutomaticTransfer(AutomaticTransfer automaticTransfer) {
    AutomaticTransfer out = _automaticTransfers.putIfAbsent(automaticTransfer.wallet.address, () => automaticTransfer);
    notifyListeners();
    return out;
  }

  AutomaticTransfer getOrCreateAutomaticTransfer(Wallet wallet) {
    AutomaticTransfer? out = _automaticTransfers[wallet.address];
    if (out != null) {
      return out;
    }
    return addAutomaticTransfer(AutomaticTransfer(wallet));
  }

  void removeAutomaticTransfer(AutomaticTransfer automaticTransfer) {
    _automaticTransfers.remove(automaticTransfer.wallet.address);
    notifyListeners();
  }
}
