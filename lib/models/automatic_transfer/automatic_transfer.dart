import 'dart:collection';

import 'package:dpatrimony/models/automatic_transfer/beneficiary.dart';
import 'package:dpatrimony/models/automatic_transfer/time_to_wait.dart';
import 'package:dpatrimony/models/automatic_transfer/wallet.dart';
import 'package:flutter/material.dart';

class AutomaticTransfer extends ChangeNotifier {
  final Wallet _wallet;

  AutomaticTransfer(this._wallet);

  Set<Beneficiary> _beneficiaries = {};
  final TimeToWait timeToWait = TimeToWait();

  addBeneficiary(Beneficiary beneficiary) {
    _beneficiaries.add(beneficiary);
    notifyListeners();
  }

  removeBeneficiary(Beneficiary beneficiary) {
    _beneficiaries.remove(beneficiary);
    notifyListeners();
  }

  UnmodifiableListView get beneficiaries => UnmodifiableListView(_beneficiaries);

  Wallet get wallet => _wallet;
}
