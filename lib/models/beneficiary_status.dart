enum BeneficiaryStatus {
  pending_setup,
  not_verified,
  unsent_secrets,
  complete,
}
