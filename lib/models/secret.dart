import 'dart:io';

import 'package:dpatrimony/models/word_list.dart';
import 'package:dpatrimony/services/secret_service.dart';

class Secret implements Comparable<Secret> {
  String _description;
  String _dictionary;
  String _encodedSecret;
  WordList _wordList;
  List<String> _restoredWordList;
  bool loadingWords = false;
  bool invalid = false;

  Secret({required String description, required String dictionary, required WordList wordList, required String encodedSecret})
      : _description = description,
        _dictionary = dictionary,
        _wordList = wordList,
        _encodedSecret = encodedSecret,
        _restoredWordList = [];

  String get description => _description;

  String get dictionary => _dictionary;

  List<String>? get restoredWordList => _restoredWordList;

  Future<List<String>> getWordList() async {
    if (loadingWords) {
      while (loadingWords) {
        sleep(Duration(milliseconds: 300));
      }
    }
    try {
      if (_restoredWordList.isEmpty) {
        loadingWords = true;
        _restoredWordList = await SecretService().expandSecretWords(_encodedSecret, _wordList);
        if (_restoredWordList.isEmpty) {
          _restoredWordList.add('Error loading secret words');
        }
      }
    } finally {
      loadingWords = false;
    }
    return _restoredWordList;
  }

  bool operator ==(o) => o is Secret && _encodedSecret == o._encodedSecret;

  int get hashCode => _encodedSecret.hashCode;

  @override
  int compareTo(Secret other) {
    return _description.compareTo(other._description);
  }
}
