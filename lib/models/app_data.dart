import 'dart:convert';
import 'package:dpatrimony/models/beneficiary.dart';
import 'package:dpatrimony/services/todo_beneficiary.dart';

class AppData {
  Map<String, dynamic> beneficiaries;

  AppData({required this.beneficiaries});

  void add(Beneficiary beneficiary) {
    beneficiaries.addAll({beneficiary.name: beneficiary});
  }

  dynamic readAll() {
    return beneficiaries;
  }

  dynamic readOnly(String key) {
    return beneficiaries[key];
  }

  Iterable<String> readKeys() {
    return beneficiaries.keys;
  }

  String readSpecificKey(int keyChosen) {
    if((beneficiaries.length-1) >= keyChosen && keyChosen >= 0){
      List<String> keys = [];
      beneficiaries.forEach((key, value) => keys.add(key));
      return keys[keyChosen];
    }
    return '';
  }

  void deleteAll() {
    beneficiaries.clear();
  }

  void deleteOnly(String key) {
    beneficiaries.remove(key);
  }

  int getLength() {
    return beneficiaries.length;
  }
}
