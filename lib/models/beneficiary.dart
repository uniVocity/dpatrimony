import 'dart:collection';
import 'dart:convert';

import 'package:dpatrimony/models/beneficiary_status.dart';
import 'package:dpatrimony/models/secret.dart';

import 'secret.dart';
import 'package:email_validator/email_validator.dart';
import 'package:http/http.dart' as http;

class Beneficiary implements Comparable<Beneficiary> {
  final String urlEmail = "192.168.0.189:8080";

  String _name;
  String? _email;
  String? _message;
  bool _isEmailConfirmed = false;
  bool _isEmailValid = false;
  Set<Secret> _secrets;
  BeneficiaryStatus status;

  Beneficiary({required String name})
      : _name = name,
        _secrets = SplayTreeSet(),
        status = BeneficiaryStatus.pending_setup;

  String get name => _name;

  void setMessage(String? message){
    _message = message;
  }

  //it's not working, it doesn't pass the value to the variable inside the beneficiary class
  //set message(String? message) => _message;

  String get message => _message ?? '';

  bool setEmail(String? email) {
    if (email != _email) {
      _email = email;
      _isEmailConfirmed = false;
    }
    if (_email != null && EmailValidator.validate(_email.toString())) {
      _isEmailValid = EmailValidator.validate(_email!);
      requestEmailSendVerification(email.toString(), name);
    } else {
      _isEmailValid = false;
    }
    return _isEmailValid;
  }

  String? get email => _email ?? '';

  bool get isEmailValid => _isEmailValid;

  Set<Secret> get secrets => Set.unmodifiable(_secrets);

  addSecret(Secret secret) {
    _secrets.add(secret);
  }

  bool operator ==(o) => o is Beneficiary && name == o.name;

  int get hashCode => name.hashCode;

  @override
  int compareTo(Beneficiary other) {
    return name.compareTo(other.name);
  }

  void requestEmailSendVerification(String email, String name) async {
    Map<String, String> data = {
      "name": name,
      "email": email,
    };
    var response = await http.post(Uri.http(urlEmail, "validator/getVerificationCode"), headers: {"Content-Type": "application/json"}, body: json.encode(data));
    Map<String, dynamic> responseData = json.decode(response.body);
    print("Email Confirmed: " + responseData["emailConfirmed"].toString());
    _isEmailConfirmed = responseData["emailConfirmed"];
  }

  bool isEmailConfirmed() {
    Future<String> requestIsEmailConfirmed() async {
      var response = await http.get(Uri.http(urlEmail, "validator/isVerifiedByEmail", {'email': _email}));
      _isEmailConfirmed = json.decode(response.body);
      return response.body;
    }
    if (!_isEmailConfirmed)
      requestIsEmailConfirmed();

    return _isEmailConfirmed;
  }

  void merge(Beneficiary other) {
    if (this == other) {
      int originalLength = this.secrets.length;
      this._secrets.addAll(other.secrets);
      if (originalLength < this.secrets.length && this.status == BeneficiaryStatus.complete) {
        this.status = BeneficiaryStatus.unsent_secrets;
      }
    }
  }
}
